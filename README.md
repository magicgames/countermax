CounterMaxAPP
===
This is the Readme to configure Countermax


## Modules requirement

 Install Facebook Plugin

* Clone the repo

	  git clone https://github.com/Wizcorp/phonegap-facebook-plugin/
	  
* Run this command 

	  cordova -d plugin add ../phonegap-facebook-plugin --variable APP_ID="858468114271382" --variable APP_NAME="CounterMax"

Get the hash key for facebook ( Production)

	keytool -exportcert -alias androidProduccion -keystore ~/.android/my-release-key.keystore | openssl sha1 -binary | openssl base64


add this to android manifest

 	<uses-permission android:name="android.permission.CAMERA"/>
  	<uses-feature android:name="android.hardware.camera"/>
  	<uses-feature android:name="android.hardware.camera.autofocus"/>
  	<uses-feature android:glEsVersion="0x00020000" android:required="true"/>


## Android Publish

	keytool -genkey -v -keystore my-release-key.keystore -alias androidProduccion -keyalg RSA -keysize 2048 -validity 10000
	cordova build --release android
	jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/.android/my-release-key.keystore android-armv7-release-unsigned.apk androidProduccion
	/opt/android/android-sdk/build-tools/23.0.2/zipalign -v 4 android-armv7-release-unsigned.apk counterMax.apk





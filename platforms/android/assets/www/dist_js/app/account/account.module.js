(function () {
  'use strict';
  angular
    .module('counterMax.account.directives', []);
  angular
    .module('counterMax.account.controllers', []);

  angular
    .module('counterMax.account.services', []);
  angular
    .module('counterMax.account.config', []);
  angular
    .module('counterMax.account', [
      'counterMax.account.controllers',
      'counterMax.account.services',
      'counterMax.account.config',
      'counterMax.account.directives'
    ]);

})();

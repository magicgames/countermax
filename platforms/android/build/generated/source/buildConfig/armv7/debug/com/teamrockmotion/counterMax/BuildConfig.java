/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.teamrockmotion.counterMax;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.teamrockmotion.counterMax";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "armv7";
  public static final int VERSION_CODE = 182;
  public static final String VERSION_NAME = "";
}

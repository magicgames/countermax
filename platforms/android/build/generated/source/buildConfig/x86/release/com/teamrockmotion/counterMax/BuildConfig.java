/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.teamrockmotion.counterMax;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.teamrockmotion.counterMax";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "x86";
  public static final int VERSION_CODE = 184;
  public static final String VERSION_NAME = "";
}

/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.google.android.gms.analytics;

public final class R {
	public static final class attr {
		public static final int adSize = 0x7f010000;
		public static final int adSizes = 0x7f010001;
		public static final int adUnitId = 0x7f010002;
		public static final int buttonSize = 0x7f010006;
		public static final int circleCrop = 0x7f010005;
		public static final int colorScheme = 0x7f010007;
		public static final int imageAspectRatio = 0x7f010004;
		public static final int imageAspectRatioAdjust = 0x7f010003;
		public static final int scopeUris = 0x7f010008;
	}
	public static final class color {
		public static final int common_action_bar_splitter = 0x7f07000b;
		public static final int common_google_signin_btn_text_dark = 0x7f07001c;
		public static final int common_google_signin_btn_text_dark_default = 0x7f07000c;
		public static final int common_google_signin_btn_text_dark_disabled = 0x7f07000d;
		public static final int common_google_signin_btn_text_dark_focused = 0x7f07000e;
		public static final int common_google_signin_btn_text_dark_pressed = 0x7f07000f;
		public static final int common_google_signin_btn_text_light = 0x7f07001d;
		public static final int common_google_signin_btn_text_light_default = 0x7f070010;
		public static final int common_google_signin_btn_text_light_disabled = 0x7f070011;
		public static final int common_google_signin_btn_text_light_focused = 0x7f070012;
		public static final int common_google_signin_btn_text_light_pressed = 0x7f070013;
		public static final int common_plus_signin_btn_text_dark = 0x7f07001e;
		public static final int common_plus_signin_btn_text_dark_default = 0x7f070014;
		public static final int common_plus_signin_btn_text_dark_disabled = 0x7f070015;
		public static final int common_plus_signin_btn_text_dark_focused = 0x7f070016;
		public static final int common_plus_signin_btn_text_dark_pressed = 0x7f070017;
		public static final int common_plus_signin_btn_text_light = 0x7f07001f;
		public static final int common_plus_signin_btn_text_light_default = 0x7f070018;
		public static final int common_plus_signin_btn_text_light_disabled = 0x7f070019;
		public static final int common_plus_signin_btn_text_light_focused = 0x7f07001a;
		public static final int common_plus_signin_btn_text_light_pressed = 0x7f07001b;
	}
	public static final class drawable {
		public static final int common_full_open_on_phone = 0x7f02002f;
		public static final int common_google_signin_btn_icon_dark = 0x7f020030;
		public static final int common_google_signin_btn_icon_dark_disabled = 0x7f020031;
		public static final int common_google_signin_btn_icon_dark_focused = 0x7f020032;
		public static final int common_google_signin_btn_icon_dark_normal = 0x7f020033;
		public static final int common_google_signin_btn_icon_dark_pressed = 0x7f020034;
		public static final int common_google_signin_btn_icon_light = 0x7f020035;
		public static final int common_google_signin_btn_icon_light_disabled = 0x7f020036;
		public static final int common_google_signin_btn_icon_light_focused = 0x7f020037;
		public static final int common_google_signin_btn_icon_light_normal = 0x7f020038;
		public static final int common_google_signin_btn_icon_light_pressed = 0x7f020039;
		public static final int common_google_signin_btn_text_dark = 0x7f02003a;
		public static final int common_google_signin_btn_text_dark_disabled = 0x7f02003b;
		public static final int common_google_signin_btn_text_dark_focused = 0x7f02003c;
		public static final int common_google_signin_btn_text_dark_normal = 0x7f02003d;
		public static final int common_google_signin_btn_text_dark_pressed = 0x7f02003e;
		public static final int common_google_signin_btn_text_light = 0x7f02003f;
		public static final int common_google_signin_btn_text_light_disabled = 0x7f020040;
		public static final int common_google_signin_btn_text_light_focused = 0x7f020041;
		public static final int common_google_signin_btn_text_light_normal = 0x7f020042;
		public static final int common_google_signin_btn_text_light_pressed = 0x7f020043;
		public static final int common_ic_googleplayservices = 0x7f020044;
		public static final int common_plus_signin_btn_icon_dark = 0x7f020045;
		public static final int common_plus_signin_btn_icon_dark_disabled = 0x7f020046;
		public static final int common_plus_signin_btn_icon_dark_focused = 0x7f020047;
		public static final int common_plus_signin_btn_icon_dark_normal = 0x7f020048;
		public static final int common_plus_signin_btn_icon_dark_pressed = 0x7f020049;
		public static final int common_plus_signin_btn_icon_light = 0x7f02004a;
		public static final int common_plus_signin_btn_icon_light_disabled = 0x7f02004b;
		public static final int common_plus_signin_btn_icon_light_focused = 0x7f02004c;
		public static final int common_plus_signin_btn_icon_light_normal = 0x7f02004d;
		public static final int common_plus_signin_btn_icon_light_pressed = 0x7f02004e;
		public static final int common_plus_signin_btn_text_dark = 0x7f02004f;
		public static final int common_plus_signin_btn_text_dark_disabled = 0x7f020050;
		public static final int common_plus_signin_btn_text_dark_focused = 0x7f020051;
		public static final int common_plus_signin_btn_text_dark_normal = 0x7f020052;
		public static final int common_plus_signin_btn_text_dark_pressed = 0x7f020053;
		public static final int common_plus_signin_btn_text_light = 0x7f020054;
		public static final int common_plus_signin_btn_text_light_disabled = 0x7f020055;
		public static final int common_plus_signin_btn_text_light_focused = 0x7f020056;
		public static final int common_plus_signin_btn_text_light_normal = 0x7f020057;
		public static final int common_plus_signin_btn_text_light_pressed = 0x7f020058;
	}
	public static final class id {
		public static final int adjust_height = 0x7f0c0000;
		public static final int adjust_width = 0x7f0c0001;
		public static final int auto = 0x7f0c0006;
		public static final int dark = 0x7f0c0007;
		public static final int icon_only = 0x7f0c0003;
		public static final int light = 0x7f0c0008;
		public static final int none = 0x7f0c0002;
		public static final int normal = 0x7f0c0012;
		public static final int standard = 0x7f0c0004;
		public static final int wide = 0x7f0c0005;
	}
	public static final class integer {
		public static final int google_play_services_version = 0x7f090000;
	}
	public static final class raw {
		public static final int gtm_analytics = 0x7f050000;
	}
	public static final class string {
		public static final int accept = 0x7f0a0000;
		public static final int auth_google_play_services_client_facebook_display_name = 0x7f0a0003;
		public static final int auth_google_play_services_client_google_display_name = 0x7f0a0004;
		public static final int common_android_wear_notification_needs_update_text = 0x7f0a0022;
		public static final int common_android_wear_update_text = 0x7f0a0023;
		public static final int common_android_wear_update_title = 0x7f0a0024;
		public static final int common_google_play_services_api_unavailable_text = 0x7f0a0025;
		public static final int common_google_play_services_enable_button = 0x7f0a0026;
		public static final int common_google_play_services_enable_text = 0x7f0a0027;
		public static final int common_google_play_services_enable_title = 0x7f0a0028;
		public static final int common_google_play_services_error_notification_requested_by_msg = 0x7f0a0029;
		public static final int common_google_play_services_install_button = 0x7f0a002a;
		public static final int common_google_play_services_install_text_phone = 0x7f0a002b;
		public static final int common_google_play_services_install_text_tablet = 0x7f0a002c;
		public static final int common_google_play_services_install_title = 0x7f0a002d;
		public static final int common_google_play_services_invalid_account_text = 0x7f0a002e;
		public static final int common_google_play_services_invalid_account_title = 0x7f0a002f;
		public static final int common_google_play_services_needs_enabling_title = 0x7f0a0030;
		public static final int common_google_play_services_network_error_text = 0x7f0a0031;
		public static final int common_google_play_services_network_error_title = 0x7f0a0032;
		public static final int common_google_play_services_notification_needs_update_title = 0x7f0a0033;
		public static final int common_google_play_services_notification_ticker = 0x7f0a0034;
		public static final int common_google_play_services_sign_in_failed_text = 0x7f0a0035;
		public static final int common_google_play_services_sign_in_failed_title = 0x7f0a0036;
		public static final int common_google_play_services_unknown_issue = 0x7f0a0037;
		public static final int common_google_play_services_unsupported_text = 0x7f0a0038;
		public static final int common_google_play_services_unsupported_title = 0x7f0a0039;
		public static final int common_google_play_services_update_button = 0x7f0a003a;
		public static final int common_google_play_services_update_text = 0x7f0a003b;
		public static final int common_google_play_services_update_title = 0x7f0a003c;
		public static final int common_google_play_services_updating_text = 0x7f0a003d;
		public static final int common_google_play_services_updating_title = 0x7f0a003e;
		public static final int common_open_on_phone = 0x7f0a003f;
		public static final int common_signin_button_text = 0x7f0a0040;
		public static final int common_signin_button_text_long = 0x7f0a0041;
		public static final int create_calendar_message = 0x7f0a0042;
		public static final int create_calendar_title = 0x7f0a0043;
		public static final int decline = 0x7f0a0044;
		public static final int store_picture_message = 0x7f0a0048;
		public static final int store_picture_title = 0x7f0a0049;
	}
	public static final class style {
		public static final int Theme_IAPTheme = 0x7f0b0000;
	}
	public static final class styleable {
		public static final int[] AdsAttrs = { 0x7f010000, 0x7f010001, 0x7f010002 };
		public static final int AdsAttrs_adSize = 0;
		public static final int AdsAttrs_adSizes = 1;
		public static final int AdsAttrs_adUnitId = 2;
		public static final int[] LoadingImageView = { 0x7f010003, 0x7f010004, 0x7f010005 };
		public static final int LoadingImageView_circleCrop = 2;
		public static final int LoadingImageView_imageAspectRatio = 1;
		public static final int LoadingImageView_imageAspectRatioAdjust = 0;
		public static final int[] SignInButton = { 0x7f010006, 0x7f010007, 0x7f010008 };
		public static final int SignInButton_buttonSize = 0;
		public static final int SignInButton_colorScheme = 1;
		public static final int SignInButton_scopeUris = 2;
	}
}

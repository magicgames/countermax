/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.controllers')
    .controller('AuthCtrl', AuthCtrl);

  AuthCtrl.$inject = ['$scope', '$state', '$cordovaFacebook','$cordovaGoogleAds', 'endpointsService', 'storageService', 'authService'];

  /**
   * @namespace AccountCtrl
   */
  function AuthCtrl($scope, $state, $cordovaFacebook,$cordovaGoogleAds,endpointsService, storageService, authService) {

    var vm = this;
    vm.facebookLogin = facebookLogin;
    vm.normalLogin = normalLogin;
    activate();


    function activate() {
      vm.email = null;
      vm.password = null;
      $cordovaGoogleAds.hideBanner();
      if (authService.isAuthenticated() === true) {
        var checkTutorial = storageService.get('checkTutorial', 'false');
        if (checkTutorial=='true') {
          $state.go('tab.tutorial');

        } else {
          $state.go('tab.video');

        }


      }
    }


    function normalLogin() {
      var userObject = {
        email: vm.email,
        password: vm.password

      };
      endpointsService.loginRegister(userObject);

    }

    function facebookLogin() {
      $cordovaFacebook.login(["public_profile", "email", "user_friends"]).then(getAccessToken, errorFacebookLogin);
    }


    function getAccessToken(userData) {

      $cordovaFacebook.getAccessToken().then(convertToken, errorAccessToken);
    }

    function errorFacebookLogin(error) {
      console.log(error);
    }

    function convertToken(token) {
      endpointsService.convertFacebookToken(token);
    }

    function errorAccessToken(error) {
      console.log(error);
    }


  }
})();

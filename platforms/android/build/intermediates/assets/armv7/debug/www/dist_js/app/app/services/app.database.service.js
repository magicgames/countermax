/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('databaseService', databaseService);

  databaseService.$inject = ['$cordovaSQLite', '$window'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function databaseService($cordovaSQLite, $window) {
    var db = null;
    return {
      openDb:openDb,
      createTableVisit: createTableVisit,
      addVisit: addVisit,
      todayVisit: todayVisit,
      monthVisit: monthVisit,
      globalVisit: globalVisit,
      closeDb: closeDb

    };

    function openDb() {
      if(window.cordova) {
        // App syntax
        db = $cordovaSQLite.openDB({name: "my.db", androidDatabaseImplementation: 2, androidLockWorkaround: 1});
      } else {
        // Ionic serve syntax
        db = window.openDatabase("my.db", "1.0", "My app", -1);
      }
    }

    function closeDb() {
      var db = accessDB();
      db.close();
    }

    /**
     *
     * @returns promise
     */
    function createTableVisit() {

      var query = "CREATE TABLE if not exists stats " +
        "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
        "date DATETIME DEFAULT (CURRENT_TIMESTAMP))";
      return executeSql(query);
    }

    function addVisit() {
      var query = "insert into stats DEFAULT VALUES";
      return executeSql(query);

    }

    function todayVisit() {
      var queryToday = "select count() as num from stats  " +
        "where date between strftime('%Y-%m-%d',date('now')) " +
        "and strftime('%Y-%m-%d',date('now','+1 day')) group by strftime('%Y-%m-%d', date)";
      return executeSql(queryToday);
    }


    function monthVisit() {
      var queryMonth = "select count() as num from stats  " +
        "where date between strftime('%Y-%m',date('now')) " +
        "and strftime('%Y-%m',date('now','+1 month')) group by strftime('%Y-%m', date);";
      return executeSql(queryMonth);
    }


    function globalVisit() {

      var queryGlobal = "select count() as num from stats;";
      return executeSql(queryGlobal);
    }

    function executeSql(sql) {
      var db = accessDB();
      return $cordovaSQLite.execute(db, sql);
    }

    function accessDB() {
     if (db===null) {
       openDb();
     }
      return db;
    }

  }
})();

/**
 * DashCtrl
 * @namespace counterMax.visitas.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.visitas.controllers')
    .controller('VisitasCtrl', VisitasCtrl);

  VisitasCtrl.$inject = ['$scope', '$cordovaGoogleAds', 'databaseService'];

  /**
   * @namespace HotelBookingController
   */
  function VisitasCtrl($scope, $cordovaGoogleAds, databaseService) {

    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.visitas.controllers.VisitasCtrl
     */
    function activate() {
      $cordovaGoogleAds.showInterstitial();
      databaseService.todayVisit().then(visitasHoy, function (e) {
        console.log(e);
      });
      databaseService.monthVisit().then(visitasMes, function (e) {
        console.log(e);
      });
      databaseService.globalVisit().then(visitasGlobal, function (e) {
        console.log(e);
      });
    }


    function visitasHoy(res) {
      var visit = 0;
      if (res.rows.item(0) !== undefined) {
        visit = res.rows.item(0).num;
      }
      $scope.visitasHoy = visit;
    }

    function visitasMes(res) {
      var visit = 0;
      if (res.rows.item(0) !== undefined) {
        visit = res.rows.item(0).num;
      }
      $scope.visitasMes = visit;
    }

    function visitasGlobal(res) {
      $scope.visitasTotal = res.rows.item(0).num;
    }


  }
})();

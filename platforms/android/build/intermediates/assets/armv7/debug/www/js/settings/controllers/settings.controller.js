/**
 * DashCtrl
 * @namespace counterMax.settings.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.settings.controllers')
    .controller('SettingsCtrl', SettingsCtrl);

  SettingsCtrl.$inject = ['$scope', '$cordovaPreferences', '$cordovaNativeAudio','$cordovaToast','$filter','$state','$cordovaGoogleAds','authService'];

  /**
   * @namespace AccountCtrl
   */
  function SettingsCtrl($scope, $cordovaPreferences, $cordovaNativeAudio,$cordovaToast,$filter,$state,$cordovaGoogleAds,authService) {

    $scope.settings = {};
    var $translate = $filter('translate');
    $scope.saveOptions = saveOptions;
    $scope.updateSound = updateSound;
    activate();


    function activate() {

      if (authService.isAuthenticated() === false) {
        $state.go('auth');

      } else {
        /**
         * Definimos los valores por defecto
         */
        $cordovaGoogleAds.showBanner(8);
        $cordovaPreferences.fetch('analyticsID').success(function (value) {
            $scope.settings.analyticsID = value;
          })
          .error(function (error) {
            alert("Error: " + error);
          });

        $cordovaPreferences.fetch('dis').success(function (value) {
            $scope.settings.disposition = value;
          })
          .error(function (error) {
            alert("Error: " + error);
          });


        $cordovaPreferences.fetch('sound')
          .success(function (value) {
            $scope.settings.sound = value;
          })
          .error(function (error) {
            $scope.settings.sound = 'sounds/Ariel.mp3';

          });

      }

    }

    function updateSound() {
      $cordovaNativeAudio.unload('click').then(successUnloadSound, failUnloadSound);
    }

    function saveOptions() {
      $cordovaPreferences.store('analyticsID', $scope.settings.analyticsID);
      $cordovaPreferences.store('dis', $scope.settings.disposition);
      $cordovaPreferences.store('sound', $scope.settings.sound);
      $cordovaToast.showLongBottom($translate('SETTINGS_SAVE')).then(function (success) {$state.go('tab.video');}, function (error) {

      });

    }

    function successUnloadSound(result) {

      $cordovaNativeAudio.preloadSimple('click', $scope.settings.sound).then(function (msg) {
        $cordovaNativeAudio.play('click');
      }, function (error) {
        alert(error);
      });


    }

    function failUnloadSound(err) {

      $cordovaNativeAudio.preloadSimple('click', $scope.settings.sound).then(function (msg) {
        $cordovaNativeAudio.play('click');
      }, function (error) {
        alert(error);
      });
    }


  }
})();

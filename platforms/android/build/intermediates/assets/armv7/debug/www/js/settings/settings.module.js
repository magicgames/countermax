(function () {
  'use strict';
  angular
    .module('counterMax.settings.directives', []);
  angular
    .module('counterMax.settings.controllers', []);

  angular
    .module('counterMax.settings.services', []);
  angular
    .module('counterMax.settings.config', []);
  angular
    .module('counterMax.settings', [
      'counterMax.settings.controllers',
      'counterMax.settings.services',
      'counterMax.settings.config',
      'counterMax.settings.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.video.controllers
 *
 *return $cordovaSQLite.execute(db, queryToday).then(function (res) {
        return res.rows.item(0).num;

      }, function (err) {
        console.error(err);
        return -1
      });


 */
(function () {
  'use strict';

  angular.module('counterMax.video.controllers').controller('VideoCtrl', VideoCtrl);
  VideoCtrl.$inject = [
    '$scope',
    '$filter',
    '$sce',
    '$cordovaPreferences',
    '$cordovaNativeAudio',
    '$state',
    '$timeout',
    '$cordovaGoogleAds',
    'databaseService',
    'Animate',
    'UserMedia',
    'VisitService',
    'rfc4122',
    'authService'
  ];

  /**
   * @namespace VideoCtrl
   */
  function VideoCtrl($scope,
                     $filter,
                     $sce,
                     $cordovaPreferences,
                     $cordovaNativeAudio,
                     $state,
                     $timeout,
                     $cordovaGoogleAds,
                     databaseService,
                     Animate,
                     UserMedia,
                     VisitService,
                     rfc4122,
                     authService) {
    var $translate = $filter('translate');
    $scope.sensor1 = 0;
    $scope.sensor2 = 1;
    $scope.inicio = false;
    $scope.abierto = false;
    $scope.stream = null;
    $scope.visitasHoy = 0;
    $scope.visitasMes = 0;
    $scope.visitasTotal = 0;
    $scope.captureVideo = captureVideo;
    $scope.video = document.getElementById('monitor');


    activate();


    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.video.controllers.VideoCtrl
     */
    function activate() {

      if (authService.isAuthenticated() === false) {
        $state.go('auth');

      } else {
        $cordovaGoogleAds.showBanner(8);
        loadAnalytics();
        prepareInterface();

        $cordovaPreferences.fetch('timer').success(function (value) {
            if (value === null) {
              value = 10;
            }
            $scope.timer = parseInt(value) * 60 * 1000; // pasamos minutos a milisegundos
          })
          .error(function (error) {
            $scope.timer = 10 * 60 * 1000; // Default 5 minutos
          });

      }

    }

    function loadAnalytics() {

      /**
       * Analitics
       */
      $cordovaPreferences.fetch('analyticsID').success(function (value) {
          console.log("Analytics Value " + value);
          $scope.analyticsID = value;
        })
        .error(function (error) {
          $scope.analyticsID = null;
        });
    }

    function prepareInterface() {
      $scope.buttons = [];
      var button1 = new Image();
      var button2 = new Image();
      var buttonData1 = null;
      var buttonData2 = null;
      button1.src = "img/SquareRed.png";
      button2.src = "img/SquareGreen.png";
      $cordovaPreferences.fetch('dis').success(function (value) {

          if (value === 0) {
            buttonData1 = {name: "red", image: button1, x: 320 - 32, y: 120, w: 32, h: 32};
            buttonData2 = {name: "green", image: button2, x: 0, y: 120, w: 32, h: 32};
          } else {
            buttonData2 = {name: "red", image: button1, x: 320 - 32, y: 120, w: 32, h: 32};
            buttonData1 = {name: "green", image: button2, x: 0, y: 120, w: 32, h: 32};
          }
          $scope.buttons.push(buttonData1);
          $scope.buttons.push(buttonData2);

        })
        .error(function (error) {
          alert("Error: " + error);
        });
    }

    function captureVideo() {
      if ($scope.abierto) {
        $scope.abierto = false;
        Animate.setVideoCanvas(document.getElementById('videoCanvas'));
        $state.go('tab.visitas');
      } else {
        $scope.abierto = true;
        Animate.setVideo($scope.video);
        Animate.setBlendCanvas(document.getElementById("blendCanvas"));
        Animate.setVideoCanvas(document.getElementById('videoCanvas'));
        Animate.setLayer2Canvas(document.getElementById('layer2'));
        Animate.setButtons($scope.buttons);
        UserMedia.start().then(sucessStartAnimation, function (e) {
          console.log("ANIMATION" + e);
        });
      }

    }

    function contabilizarNuevaVisita() {

      var cid = rfc4122.v4();
      if ($scope.analyticsID === null) {
        loadAnalytics();

      }

      var visitObj = {
        v: 1,
        tid: $scope.analyticsID,
        cid: cid,
        t: 'event',
        ec: $translate('EVENTO_CATEGORY'),
        ea: $translate('EVENTO_ACCION'),
        el: $translate('EVENTO_LABEL'),
        ev: 1,
        sc: 'start',
        ds: 'app'
      };

      databaseService.addVisit();
      $cordovaNativeAudio.play('click');
      if ($scope.analyticsID !== null) {
        VisitService.addVisit(visitObj);
        $timeout(destroyVisit, $scope.timer, true, cid);
      }
    }

    function destroyVisit(cid) {
      var destroyObject = {
        v: 1,
        tid: $scope.analyticsID,
        cid: cid,
        sc: 'stop'

      };
      VisitService.destroySession(destroyObject);
    }

    function animate() {
      if ($scope.abierto) {
        Animate.draw().then(drawSuccessFn, function () {
          newFrame();
        });
      }
    }


    function sucessStartAnimation(stream) {
      window.stream = stream; // stream available to console for dev
      if (window.URL) {
        $scope.videostream = $sce.trustAsResourceUrl(window.URL.createObjectURL(stream));
      } else {
        $scope.videostream = $sce.trustAsResourceUrl(stream);
      }
      animate();

    }

    function newFrame() {
      if ($scope.abierto) {
        ionic.requestAnimationFrame(animate);
      }
    }

    function drawSuccessFn(data) {
      if (data.status == 1) {

        Animate.blender().then(blenderSuccessFn, function () {
          console.log("error blender");
        });

      } else {
        newFrame();

      }

    }

    function blenderSuccessFn(data) {

      if (data.status == 1) {
        Animate.checkAreas().then(checkAreasSuccessFn, function () {
          console.log("error areas");
        });

      }
    }

    function checkAreasSuccessFn(data) {

      if (data.status === 1) {
        if (data.btn >= 0) {
          if (data.btn === 0) {
            $scope.inicio = true;
          } else {
            if (( $scope.inicio === true) && (data.btn == 1)) {
              $scope.inicio = false;
              contabilizarNuevaVisita();
            }
          }
        }
      }
      newFrame();
    }


  }
})();

(function () {
  'use strict';
  /* @ngInject */
  videoLaunch.$inject = ['$rootScope', '$timeout'];
  function videoLaunch($rootScope, $timeout) {

    var directive = {
      restrict: 'EA',
      scope: {
        hotel: '=',
      },
      template: '<div class="video-container"></div>',
      link: link,

    };
    return directive;

    function link(scope, element, attributes) {


      function updatePosition() {
        cordova.plugins.phonertc.setVideoView({
          container: element[0],
          local: {
            position: [240, 240],
            size: [50, 50]
          }
        });

      }
    }


  }

  angular
    .module('counterMax.video.directives')
    .directive('videoView', videoLaunch);


})();

(function () {
  'use strict';

  angular.module('counterMax', [
    'ngCordova',
    'templates',
    'pascalprecht.translate',  // inject the angular-translate module
    'ionic',
    'counterMax.routes',
    'counterMax.services',
    'counterMax.controllers',
    'counterMax.video',
    'counterMax.dashboard',
    'counterMax.account',
    'counterMax.settings',
    'counterMax.visitas'



  ]);
  angular.module('counterMax.services', []);
  angular.module('counterMax.routes', []);
  angular.module('counterMax.controllers', []);


  angular.module('counterMax').config(config);
  config.$inject = ['$ionicConfigProvider', '$translateProvider'];
  angular.module('counterMax').run(run);
  run.$inject = [
    '$ionicPlatform',
    '$cordovaPushV5',
    '$rootScope',
    '$cordovaToast',
    '$ionicHistory',
    '$state',
    '$translate',
    '$filter',
    '$cordovaGoogleAds',
    'databaseService'];

  /**
   * @name config
   * @desc Configure all factories
   * @param $ionicConfigProvider
   * @param $translateProvider
   */
  function config($ionicConfigProvider, $translateProvider) {

    $ionicConfigProvider.navBar.alignTitle('center');
    $translateProvider
      .useStaticFilesLoader({
        prefix: 'locales/',
        suffix: '.json'
      })
      .registerAvailableLanguageKeys(['es', 'en'], {
        'en': 'en', 'en_GB': 'en', 'en_US': 'en',
        'es': 'es', 'es_ES': 'es', 'es_AR': 'es'
      })
      .preferredLanguage('es')
      .fallbackLanguage('en')
      .determinePreferredLanguage()
      .useSanitizeValueStrategy('escapeParameters');
  }


  /**
   * @name run
   */
  function run($ionicPlatform,
               $cordovaPushV5,
               $rootScope,
               $cordovaToast,
               $ionicHistory,
               $state,
               $translate,
               $filter,
               $cordovaGoogleAds,
               databaseService) {

    var countTimerForCloseApp = false;


    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      if (window.cordova) {
        registerBackButton($ionicPlatform, $cordovaToast, $state, $ionicHistory, $filter, countTimerForCloseApp);
        registerPush($cordovaPushV5, $rootScope);
        databaseService.createTableVisit();
        prepareTranslation($translate);

        prepareAds($cordovaGoogleAds);

      }
      if (window.localStorage.loginCorrect === "true") {
        $state.go('tab.video');

      } else {

        $state.go('auth');
      }


    });

  }

  /**
   * @name prepareTranlation
   * @desc carga el archivo de traduccion
   * @param $translate
   */
  function prepareTranslation($translate) {
    if (typeof navigator.globalization !== "undefined") {
      navigator.globalization.getPreferredLanguage(function (language) {
        var language_list = {"French": "es", "English": "en", "Spanish": "es", "Chinese": "zh", "Japanese": "ja"};
        var language_value = (language.value).split(/[\s,-]+/)[0];

        if (language_list.hasOwnProperty(language_value)) {
          var language_locale = language_list[language_value];
          $translate.use(language_locale).then(function (data) {
            console.log("SUCCESS -> " + data);
          }, function (error) {
            console.log("ERROR -> " + error);
          });
        }
      }, null);
    }

  }

  /**
   * @name prepareAds
   * @desc prepara la publicidad
   * @param $cordovaGoogleAds
   */
  function prepareAds($cordovaGoogleAds) {
    var admobid = {};
    //Banner configuration
    admobid = {
      banner: 'ca-app-pub-1481586652646755/4145805025', // or DFP format "/6253334/dfp_example_ad"
      interstitial: 'ca-app-pub-1481586652646755/1192338625'
    };
    if (ionic.Platform.isIOS()) {
      cordova.plugins.iosrtc.registerGlobals();
      admobid = {
        banner: 'ca-app-pub-1481586652646755/4145805025', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-1481586652646755/1192338625'
      };
    }
    if (ionic.Platform.isAndroid()) {
      admobid = {
        banner: 'ca-app-pub-1481586652646755/4145805025', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-1481586652646755/1192338625'
      };

    }
    if (ionic.Platform.isWindowsPhone()) {
      admobid = {
        banner: 'ca-app-pub-1481586652646755/4145805025', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-1481586652646755/1192338625'
      };
    }

    if (ionic.Platform.isWebView()) {
      admobid = {
        banner: 'ca-app-pub-1481586652646755/4145805025', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-1481586652646755/1192338625'
      };
    }
    var banner_options = {
      adSize: 'SMART_BANNER',
      width: 360, // valid when set adSize 'CUSTOM'
      height: 90, // valid when set adSize 'CUSTOM'
      position: 8,
      x: 0,       // valid when set position to POS_XY
      y: 0,       // valid when set position to POS_XY
      isTesting: false,
      autoShow: true
    };
    $cordovaGoogleAds.setOptions(banner_options).then(creobannerOK, creobannerKO);
    function creobannerOK() {
      $cordovaGoogleAds.createBanner(admobid.banner).then(function () {
        $cordovaGoogleAds.prepareInterstitial({adId: admobid.interstitial, autoShow: false, isTesting: false});
      }, creobannerKO);

    }

    function creobannerKO() {
      console.log("No puedo hacer nada");
    }


  }


  function registerBackButton($ionicPlatform, $cordovaToast, $state, $ionicHistory, $filter, countTimerForCloseApp) {
    var $translate = $filter('translate');
    $ionicPlatform.registerBackButtonAction(function (e) {
      e.preventDefault();
      function showConfirm() {
        if (countTimerForCloseApp) {
          ionic.Platform.exitApp();
        } else {
          $cordovaToast.showLongBottom($translate('CONFIRM_EXIT')).then(function (success) {
            countTimerForCloseApp = true;
            $timeout(function () {
              countTimerForCloseApp = false;
            }, 2000, true, countTimerForCloseApp);

          }, function (error) {
            // error
          });
        }

      }

      // Is there a page to go back to?
      if (($ionicHistory.backView()) && !($state.is('tab.tutorial') || $state.is('tab.video'))) {
        // Go back in history
        $ionicHistory.backView().go();
      } else {
        // This is the last page: Show confirmation popup
        showConfirm();
      }

      return false;
    }, 101);
  }

  /**
   * Controla el sistema de push
   * @param $cordovaPushV5
   * @param $rootScope
   */
  function registerPush($cordovaPushV5, $rootScope) {

    var pushOptions = {
      android: {
        senderID: "822255661242",
        icon: "icon",
        iconColor: "#008e94"
      },
      ios: {
        alert: "true",
        badge: "true",
        sound: "true"
      },
      windows: {}
    };


    $cordovaPushV5.initialize(pushOptions);
    $cordovaPushV5.register().then(function (token) {
      console.log('$cordovaPushV5:REGISTERED', token);
    }, function (err) {
      console.error('$cordovaPushV5:REGISTER_ERROR', err);
    });


    $rootScope.$on('$cordovaPushV5:notificationReceived', function (event, notification) {
      switch (notification.event) {
        case 'registered':
          if (notification.regid.length > 0) {
            alert('registration ID = ' + notification.regid);
          }
          break;

        case 'message':
          // this is the actual push notification. its format depends on the data model from the push server
          alert('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
          break;

        case 'error':
          alert('GCM error = ' + notification.msg);
          break;

        default:
          alert('An unknown GCM event has occurred');
          break;
      }
    });


  }
})();

angular.module("templates", []).run(["$templateCache", function($templateCache) {$templateCache.put("login.html","<ion-view title=\"COUNTERMAX\">\n  <ion-content class=\"padding\">\n    <img src=\"img/ojo.png\" style=\"width:100%\">\n    <div class=\"list\">\n      <button class=\"button button-full icon-left ion-social-facebook button-facebook\" ng-click=\"auth.facebookLogin()\">\n        <small translate=\"LOGIN_FACEBOOK\"></small>\n      </button>\n    </div>\n\n    <form>\n    <div class=\"list card\">\n      <div class=\"item item-divider\"><small translate=\"LOGIN_REGISTER\"></small></div>\n      <div class=\"item item-body\">\n        <div>\n          <label class=\"item item-input\">\n            <span class=\"input-label\">Email</span>\n            <input type=\"email\"  ng-model=\"auth.email\" required>\n          </label>\n\n          <label class=\"item item-input item-icon-right\">\n            <span class=\"input-label\" translate=\"PASSWORD\"></span>\n            <i class=\"icon ion-eye\" on-touch=\"showPassword = !showPassword\" ng-show=\"showPassword\" ></i>\n            <i class=\"icon ion-eye-disabled placeholder-icon\" on-touch=\"showPassword = !showPassword\" ng-hide=\"showPassword\"></i>\n            <input type=\"password\" id=\"password\" placeholder=\"{{PASSWORD |translate}}\" ng-model=\"auth.password\" ng-hide=\"showPassword\" required>\n            <input type=\"text\" id=\"passwordRaw\" placeholder=\"{{PASSWORD |translate}}\" ng-model=\"auth.password\" ng-show=\"showPassword\" required>\n          </label>\n          <label class=\"item item-input\">\n            <button class=\"button button-full button-stable\" ng-click=\"auth.normalLogin()\">\n             {{\'ENTRAR_REGISTRO\' | translate}}\n            </button>\n          </label>\n        </div>\n      </div>\n    </div>\n\n    </form>\n  </ion-content>\n\n</ion-view>\n");
$templateCache.put("tab-settings.html","<ion-view>\n  <ion-nav-title>\n    <small translate=\"MENU_SETTINGS\"></small>\n  </ion-nav-title>\n  <ion-content class=\"padding\">\n    <div class=\"item item-divider\">\n      Configuración básica\n    </div>\n    <label class=\"item item-input\">\n      <span class=\"input-label\">Analytics ID</span>\n      <input type=\"text\" ng-model=\"settings.analyticsID\" placeholder=\"UA-00000-00\">\n    </label>\n\n    <label class=\"item item-input item-select\">\n      <div class=\"input-label\">\n        Elija Sonido\n      </div>\n      <select ng-model=\"settings.sound\" ng-change=\"updateSound()\">\n        <option value=\"sounds/Ariel.mp3\">Ariel</option>\n        <option value=\"sounds/Carme.mp3\">Carme</option>\n        <option value=\"sounds/Ceres.mp3\">Ceres</option>\n        <option value=\"sounds/Elara.mp3\">Elara</option>\n        <option value=\"sounds/Europa.mp3\">Europa</option>\n        <option value=\"sounds/Iapetus.mp3\">Iapetus</option>\n        <option value=\"sounds/Io.mp3\">Io</option>\n        <option value=\"sounds/Rhea.mp3\">Rhea</option>\n        <option value=\"sounds/Salacia.mp3\">Salacia</option>\n        <option value=\"sounds/Tethys.mp3\">Tethys</option>\n        <option value=\"sounds/Titan.mp3\">Titan</option>\n\n\n      </select>\n    </label>\n\n    <label class=\"item item-input item-select\">\n      <div class=\"input-label\">\n        Elija disposición\n      </div>\n      <select ng-model=\"settings.disposition\">\n        <option value=0>Derecha</option>\n        <option value=1>Izquierda</option>\n\n\n      </select>\n    </label>\n    <button class=\"button button-full button-stable\" ng-click=\"saveOptions()\">\n      Grabar\n    </button>\n  </ion-content>\n</ion-view>\n");
$templateCache.put("tab-tutorial.html","<ion-view view-title=\"Dashboard\">\n  <ion-nav-title>\n    <small translate=\"MENU_TUTORIAL\"></small>\n  </ion-nav-title>\n  <ion-content>\n    <ion-slide-box on-slide-changed=\"slideHasChanged($index)\">\n      <ion-slide class=\"padding\">\n        <h3><i class=\"ion-ios-gear-outline\" style=\"margin-right:5px\"></i>{{\"MENU_SETTINGS\"|translate}}</h3>\n        <div class=\"image_tutorial\">\n          <img src=\"img/tutorial_2.png\">\n        </div>\n        <span data-ng-bind-html=\"\'SLIDE_1_TEXT\'|translate\"></span>\n\n      </ion-slide>\n      <ion-slide class=\"padding\">\n        <h3><i class=\"ion-ios-eye-outline\" style=\"margin-right:4px\"></i>{{\"MENU_VIDEO\"|translate}}</h3>\n        <div class=\"image_tutorial\">\n        <img src=\"img/tutorial_1.png\">\n        </div>\n        <span data-ng-bind-html=\"\'SLIDE_2_TEXT\'|translate\"></span>\n\n      </ion-slide>\n      <ion-slide class=\"padding\">\n        <h3><i class=\"ion-ios-pie-outline\" style=\"margin-right:4px\"></i>{{\"MENU_VISITAS\"|translate}}</h3>\n        <div class=\"image_tutorial\">\n          <img src=\"img/tutorial_3.png\">\n        </div>\n        <span data-ng-bind-html=\"\'SLIDE_3_TEXT\'|translate\"></span>\n         </ion-slide>\n    </ion-slide-box>\n\n  </ion-content>\n\n</ion-view>\n");
$templateCache.put("tab-video.html","<ion-view>\n  <ion-nav-title>\n    <small translate=\"MENU_VIDEO\"></small>\n  </ion-nav-title>\n  <ion-content>\n    <img src=\"img/ojo.png\" style=\"width:100%\" ng-hide=\"abierto\">\n    <div class=\"center\" ng-show=\"abierto\">\n      <video id=\"monitor\" ng-src=\"{{videostream}}\" autoplay muted></video>\n\n      <div id=\"canvasLayers\">\n        <canvas id=\"videoCanvas\" width=\"320\" height=\"240\"></canvas>\n        <canvas id=\"layer2\" width=\"320\" height=\"240\"></canvas>\n      </div>\n\n\n      <canvas id=\"blendCanvas\"></canvas>\n    </div>\n    <button class=\"button button-full button-stable\" ng-click=\"captureVideo()\">\n      <small translate=\"VIDEO_CERRAR_TIENDA\" ng-show=\"abierto\"></small>\n      <small translate=\"VIDEO_ABRIR_TIENDA\" ng-hide=\"abierto\"></small>\n    </button>\n  </ion-content>\n</ion-view>\n");
$templateCache.put("tab-visitas.html","<ion-view>\n  <ion-nav-title>\n    <small translate=\"MENU_VISITAS\"></small>\n  </ion-nav-title>\n  <ion-content class=\"padding\">\n    <div class=\"list card\">\n      <div class=\"item item-divider\"> <small translate=\"VISITAS_DIARIAS\"></small></div>\n      <div class=\"item item-body\">\n        <div>\n          <strong>{{visitasHoy}}</strong>\n        </div>\n      </div>\n    </div>\n    <div class=\"list card\">\n      <div class=\"item item-divider\"><small translate=\"VISITAS_MES\"></small></div>\n      <div class=\"item item-body\">\n        <div>\n          <strong>{{visitasMes}}</strong>\n        </div>\n      </div>\n    </div>\n    <div class=\"list card\">\n      <div class=\"item item-divider\"><small translate=\"VISITAS_TOTALES\"></small></div>\n      <div class=\"item item-body\">\n        <div>\n          <strong>{{visitasTotal}}</strong>\n        </div>\n      </div>\n    </div>\n  </ion-content>\n\n</ion-view>\n");
$templateCache.put("tabs.html","<!--\nCreate tabs with an icon and label, using the tabs-positive style.\nEach tab\'s child <ion-nav-view> directive will have its own\nnavigation history that also transitions its views in and out.\n-->\n<ion-tabs class=\"tabs-icon-top tabs-color-active-positive\">\n\n  <!-- Dashboard Tab -->\n  <ion-tab title=\"{{ \'MENU_TUTORIAL\' | translate }}\" icon-off=\"ion-ios-list-outline\" icon-on=\"ion-ios-list\" href=\"#/tab/tutorial\">\n    <ion-nav-view name=\"tab-tutorial\"></ion-nav-view>\n  </ion-tab>\n\n\n  <!-- Account Tab -->\n  <ion-tab title=\"{{ \'MENU_SETTINGS\' | translate }}\" icon-off=\"ion-ios-gear-outline\" icon-on=\"ion-ios-gear\" href=\"#/tab/settings\">\n    <ion-nav-view name=\"tab-settings\"></ion-nav-view>\n  </ion-tab>\n\n  <ion-tab title=\"{{ \'MENU_VIDEO\' | translate }}\" icon-off=\"ion-ios-eye-outline\" icon-on=\"ion-ios-eye\" href=\"#/tab/video\">\n    <ion-nav-view name=\"tab-video\"></ion-nav-view>\n  </ion-tab>\n\n\n\n  <!-- Chats Tab -->\n  <ion-tab title=\"{{ \'MENU_VISITAS\' | translate }}\" icon-off=\"ion-ios-pie-outline\" icon-on=\"ion-ios-pie\" href=\"#/tab/visitas\">\n    <ion-nav-view name=\"tab-visitas\"></ion-nav-view>\n  </ion-tab>\n\n\n\n</ion-tabs>\n");}]);
(function () {
  'use strict';

  angular
    .module('counterMax.routes')
    .config(config);

  config.$inject = ['$stateProvider','$urlRouterProvider'];

  /**
   * @name config
   * @desc Enable HTML5 routing
   */
  function config($stateProvider, $urlRouterProvider) {



    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
      .state('auth', {
        url: '/auth',
        cache:false,
        templateUrl: 'login.html',
        controller:'AuthCtrl as auth'
      })

      .state('intro', {
        url: '/',
        cache:false,
        template:'<div></div>',
        controller:'DefaultController'
      })



      // setup an abstract state for the tabs directive
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'tabs.html'

      })

      // Each tab has its own nav history stack:

      .state('tab.tutorial', {
        url: '/tutorial',
        views: {
          'tab-tutorial': {
            templateUrl: 'tab-tutorial.html',
            controller: 'DashCtrl'
          }
        }
      })
      .state('tab.visitas', {
        url: '/visitas',
        cache:false,
        views: {
          'tab-visitas': {
            templateUrl: 'tab-visitas.html',
            controller: 'VisitasCtrl'
          }
        }
      })

      .state('tab.video', {
        url: '/video',
        views: {
          'tab-video': {
            templateUrl: 'tab-video.html',
            controller: 'VideoCtrl'
          }
        }
      })
      .state('tab.settings', {
        url: '/settings',
        views: {
          'tab-settings': {
            templateUrl: 'tab-settings.html',
            controller: 'SettingsCtrl'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');


  }
})();

/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.controllers')
    .controller('DefaultController', DefaultController);

  DefaultController.$inject = ['$scope', '$state','storageService','authService'];

  /**
   * @namespace AccountCtrl
   */
  function DefaultController($scope, $state,storageService,authService) {

    activate();
    function activate() {

      if (authService.isAuthenticated() === true) {
        var checkTutorial = storageService.get('checkTutorial', false);
        if (checkTutorial) {
          $state.go('tab.tutorial');

        } else {
          $state.go('tab.video');

        }


      } else {

        $state.go('auth');
      }
    }

  }
})();


/*

 .then(function(success) {
 window.localStorage.loginCorrect="true";
 if(window.localStorage.didTutorial === "true") {
 $state.go('tab.video');
 } else {

 $state.go('tab.tutorial');
 }



 }, function (error) {
 console.log(error);
 });




 function getAccessToken(userData) {
 alert("UserInfo: " + JSON.stringify(userData));
 facebookConnectPlugin.getAccessToken(function(token) {
 alert("Token: " + token);
 }, function(err) {
 alert("Could not get access token: " + err);
 });




 Oauth.convertFacebookToken(facebookToken,options)

 */

/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.controllers')
    .controller('AuthCtrl', AuthCtrl);

  AuthCtrl.$inject = ['$scope', '$state', '$cordovaFacebook','$cordovaGoogleAds', 'endpointsService', 'storageService', 'authService'];

  /**
   * @namespace AccountCtrl
   */
  function AuthCtrl($scope, $state, $cordovaFacebook,$cordovaGoogleAds,endpointsService, storageService, authService) {

    var vm = this;
    vm.facebookLogin = facebookLogin;
    vm.normalLogin = normalLogin;
    activate();


    function activate() {
      vm.email = null;
      vm.password = null;
      $cordovaGoogleAds.hideBanner();
      if (authService.isAuthenticated() === true) {
        var checkTutorial = storageService.get('checkTutorial', 'false');
        if (checkTutorial=='true') {
          $state.go('tab.tutorial');

        } else {
          $state.go('tab.video');

        }


      }
    }


    function normalLogin() {
      var userObject = {
        email: vm.email,
        password: vm.password

      };
      endpointsService.loginRegister(userObject);

    }

    function facebookLogin() {
      $cordovaFacebook.login(["public_profile", "email", "user_friends"]).then(getAccessToken, errorFacebookLogin);
    }


    function getAccessToken(userData) {

      $cordovaFacebook.getAccessToken().then(convertToken, errorAccessToken);
    }

    function errorFacebookLogin(error) {
      console.log(error);
    }

    function convertToken(token) {
      endpointsService.convertFacebookToken(token);
    }

    function errorAccessToken(error) {
      console.log(error);
    }


  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('databaseService', databaseService);

  databaseService.$inject = ['$cordovaSQLite', '$window'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function databaseService($cordovaSQLite, $window) {
    var db = null;
    return {
      openDb:openDb,
      createTableVisit: createTableVisit,
      addVisit: addVisit,
      todayVisit: todayVisit,
      monthVisit: monthVisit,
      globalVisit: globalVisit,
      closeDb: closeDb

    };

    function openDb() {
      if(window.cordova) {
        // App syntax
        db = $cordovaSQLite.openDB({name: "my.db", androidDatabaseImplementation: 2, androidLockWorkaround: 1});
      } else {
        // Ionic serve syntax
        db = window.openDatabase("my.db", "1.0", "My app", -1);
      }
    }

    function closeDb() {
      var db = accessDB();
      db.close();
    }

    /**
     *
     * @returns promise
     */
    function createTableVisit() {

      var query = "CREATE TABLE if not exists stats " +
        "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
        "date DATETIME DEFAULT (CURRENT_TIMESTAMP))";
      return executeSql(query);
    }

    function addVisit() {
      var query = "insert into stats DEFAULT VALUES";
      return executeSql(query);

    }

    function todayVisit() {
      var queryToday = "select count() as num from stats  " +
        "where date between strftime('%Y-%m-%d',date('now')) " +
        "and strftime('%Y-%m-%d',date('now','+1 day')) group by strftime('%Y-%m-%d', date)";
      return executeSql(queryToday);
    }


    function monthVisit() {
      var queryMonth = "select count() as num from stats  " +
        "where date between strftime('%Y-%m',date('now')) " +
        "and strftime('%Y-%m',date('now','+1 month')) group by strftime('%Y-%m', date);";
      return executeSql(queryMonth);
    }


    function globalVisit() {

      var queryGlobal = "select count() as num from stats;";
      return executeSql(queryGlobal);
    }

    function executeSql(sql) {
      var db = accessDB();
      return $cordovaSQLite.execute(db, sql);
    }

    function accessDB() {
     if (db===null) {
       openDb();
     }
      return db;
    }

  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('endpointsService', endpointsService);

  endpointsService.$inject = ['$http', '$state', 'storageService'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function endpointsService($http, $state, storageService) {
    var config = {
      baseUrl: 'https://www.countermax.com',
      clientId: 'fnu10PTJnT8EJCYuRKMecawwJH6B8gVKAEFi48Do',
      clientSecret: null,
      convertTokenPath: '/auth/convert-token',
      grantPath: '/auth/token',
      revokePath: '/auth/revoke-token',
      loginUrl: '/account/login'

    };
    var globalUserObject;

    return {

      convertFacebookToken: convertFacebookToken,
      loginRegister: loginRegister
    };

    function convertFacebookToken(token) {

      var content = {
        client_id: config.clientId,
        grant_type: 'convert_token',
        backend: 'facebook',
        token: token
      };


      $http({
        url: config.baseUrl + config.convertTokenPath,
        method: 'POST',
        params: content,
        paramSerializer: '$httpParamSerializerJQLike'
      }).then(successTokenCallback, errorTokenCallback);
    }


    function successTokenCallback(response) {
      storageService.setObject('token', response);
      storageService.set('isRegistered', true);
      storageService.set('checkTutorial', true);
      $state.go('tab.tutorial');

      return true;
    }

    function errorTokenCallback(response) {
      console.log(response);
      return false;
    }


    function loginRegister(userObject) {
      globalUserObject = userObject;
      var content = {
        email: userObject.email,
        password: userObject.password
      };
      $http.post(config.baseUrl + config.loginUrl, content).then(accountToken, function (data, status, headers, config, userObject) {
          console.log(data);
        }
      );


    }

    function accountToken() {
      var content = {
        client_id: config.clientId,
        grant_type: 'password',
        username: globalUserObject.email,
        password: globalUserObject.password
      };

      $http({
        url: config.baseUrl + config.grantPath,
        method: 'POST',
        params: content,
        paramSerializer: '$httpParamSerializerJQLike'
      }).then(successTokenCallback, errorTokenCallback);
    }


  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('authService', authService);

  authService.$inject = ['storageService'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function authService(storageService) {

    return {

      isAuthenticated: isAuthenticated
    };

    function isAuthenticated() {

      var x = storageService.get('isRegistered', false);
      if (x == 'true') {

        return true;
      } else {
        return false;
      }
    }


  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('storageService', storageService);

  storageService.$inject = ['$window'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function storageService($window) {

    return {
      set:set,
      get:get,
      removeKey:removeKey,
      setObject:setObject,
      getObject:getObject

    };

    function set(key,value) {
      $window.localStorage[key] = value;
    }

    function get(key,defaultValue) {
      return $window.localStorage[key] || defaultValue;
    }

    function removeKey(key) {
      return $window.removeItem[key];
    }
    function setObject(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    }

    function getObject(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }

  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.account.directives', []);
  angular
    .module('counterMax.account.controllers', []);

  angular
    .module('counterMax.account.services', []);
  angular
    .module('counterMax.account.config', []);
  angular
    .module('counterMax.account', [
      'counterMax.account.controllers',
      'counterMax.account.services',
      'counterMax.account.config',
      'counterMax.account.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.account.controllers')
    .controller('AccountCtrl', AccountCtrl);

  AccountCtrl.$inject = ['$scope','$cordovaPreferences'];

  /**
   * @namespace AccountCtrl
   */
  function AccountCtrl($scope,$cordovaPreferences) {
    var vm = this;
    $scope.settings = {
      enableFriends: true
    };
    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.dashboard.controllers.DashCtrl
     */
    function activate() {
      $cordovaPreferences.show()
        .success(function(value) {
          alert("Success: " + value);
        })
        .error(function(error) {
          alert("Error: " + error);
        });
    }


  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.visitas.directives', []);
  angular
    .module('counterMax.visitas.controllers', []);

  angular
    .module('counterMax.visitas.services', []);
  angular
    .module('counterMax.visitas.config', []);
  angular
    .module('counterMax.visitas', [
      'counterMax.visitas.controllers',
      'counterMax.visitas.services',
      'counterMax.visitas.config',
      'counterMax.visitas.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.visitas.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.visitas.controllers')
    .controller('VisitasCtrl', VisitasCtrl);

  VisitasCtrl.$inject = ['$scope', '$cordovaGoogleAds', 'databaseService'];

  /**
   * @namespace HotelBookingController
   */
  function VisitasCtrl($scope, $cordovaGoogleAds, databaseService) {

    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.visitas.controllers.VisitasCtrl
     */
    function activate() {
      $cordovaGoogleAds.showInterstitial();
      databaseService.todayVisit().then(visitasHoy, function (e) {
        console.log(e);
      });
      databaseService.monthVisit().then(visitasMes, function (e) {
        console.log(e);
      });
      databaseService.globalVisit().then(visitasGlobal, function (e) {
        console.log(e);
      });
    }


    function visitasHoy(res) {
      var visit = 0;
      if (res.rows.item(0) !== undefined) {
        visit = res.rows.item(0).num;
      }
      $scope.visitasHoy = visit;
    }

    function visitasMes(res) {
      var visit = 0;
      if (res.rows.item(0) !== undefined) {
        visit = res.rows.item(0).num;
      }
      $scope.visitasMes = visit;
    }

    function visitasGlobal(res) {
      $scope.visitasTotal = res.rows.item(0).num;
    }


  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.dashboard.directives', []);
  angular
    .module('counterMax.dashboard.controllers', []);

  angular
    .module('counterMax.dashboard.services', []);
  angular
    .module('counterMax.dashboard.config', []);
  angular
    .module('counterMax.dashboard', [
      'counterMax.dashboard.controllers',
      'counterMax.dashboard.services',
      'counterMax.dashboard.config',
      'counterMax.dashboard.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.dashboard.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.dashboard.controllers')
    .controller('DashCtrl', DashCtrl);

  DashCtrl.$inject = ['$scope','$cordovaGoogleAds','$state','authService'];

  /**
   * @namespace HotelBookingController
   */
  function DashCtrl($scope,$cordovaGoogleAds,$state,authService) {

    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.dashboard.controllers.DashCtrl
     */
    function activate() {

      if (authService.isAuthenticated() === false) {
        $state.go('auth');

      } else {
        $cordovaGoogleAds.showBanner(8);
      }

    }



  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.video.directives', []);
  angular
    .module('counterMax.video.controllers', []);

  angular
    .module('counterMax.video.services', []);
  angular
    .module('counterMax.video.config', []);
  angular
    .module('counterMax.video', [
      'counterMax.video.controllers',
      'counterMax.video.services',
      'counterMax.video.config',
      'counterMax.video.directives',
      'uuid'
    ]);

})();

(function () {
  'use strict';
  /* @ngInject */
  videoLaunch.$inject = ['$rootScope', '$timeout'];
  function videoLaunch($rootScope, $timeout) {

    var directive = {
      restrict: 'EA',
      scope: {
        hotel: '=',
      },
      template: '<div class="video-container"></div>',
      link: link,

    };
    return directive;

    function link(scope, element, attributes) {


      function updatePosition() {
        cordova.plugins.phonertc.setVideoView({
          container: element[0],
          local: {
            position: [240, 240],
            size: [50, 50]
          }
        });

      }
    }


  }

  angular
    .module('counterMax.video.directives')
    .directive('videoView', videoLaunch);


})();

/**
 * DashCtrl
 * @namespace counterMax.video.controllers
 *
 *return $cordovaSQLite.execute(db, queryToday).then(function (res) {
        return res.rows.item(0).num;

      }, function (err) {
        console.error(err);
        return -1
      });


 */
(function () {
  'use strict';

  angular.module('counterMax.video.controllers').controller('VideoCtrl', VideoCtrl);
  VideoCtrl.$inject = [
    '$scope',
    '$filter',
    '$sce',
    '$cordovaPreferences',
    '$cordovaNativeAudio',
    '$state',
    '$timeout',
    '$cordovaGoogleAds',
    'databaseService',
    'Animate',
    'UserMedia',
    'VisitService',
    'rfc4122',
    'authService'
  ];

  /**
   * @namespace VideoCtrl
   */
  function VideoCtrl($scope,
                     $filter,
                     $sce,
                     $cordovaPreferences,
                     $cordovaNativeAudio,
                     $state,
                     $timeout,
                     $cordovaGoogleAds,
                     databaseService,
                     Animate,
                     UserMedia,
                     VisitService,
                     rfc4122,
                     authService) {
    var $translate = $filter('translate');
    $scope.sensor1 = 0;
    $scope.sensor2 = 1;
    $scope.inicio = false;
    $scope.abierto = false;
    $scope.stream = null;
    $scope.visitasHoy = 0;
    $scope.visitasMes = 0;
    $scope.visitasTotal = 0;
    $scope.captureVideo = captureVideo;
    $scope.video = document.getElementById('monitor');


    activate();


    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.video.controllers.VideoCtrl
     */
    function activate() {

      if (authService.isAuthenticated() === false) {
        $state.go('auth');

      } else {
        $cordovaGoogleAds.showBanner(8);
        loadAnalytics();
        prepareInterface();

        $cordovaPreferences.fetch('timer').success(function (value) {
            if (value === null) {
              value = 10;
            }
            $scope.timer = parseInt(value) * 60 * 1000; // pasamos minutos a milisegundos
          })
          .error(function (error) {
            $scope.timer = 10 * 60 * 1000; // Default 5 minutos
          });

      }

    }

    function loadAnalytics() {

      /**
       * Analitics
       */
      $cordovaPreferences.fetch('analyticsID').success(function (value) {
          console.log("Analytics Value " + value);
          $scope.analyticsID = value;
        })
        .error(function (error) {
          $scope.analyticsID = null;
        });
    }

    function prepareInterface() {
      $scope.buttons = [];
      var button1 = new Image();
      var button2 = new Image();
      var buttonData1 = null;
      var buttonData2 = null;
      button1.src = "img/SquareRed.png";
      button2.src = "img/SquareGreen.png";
      $cordovaPreferences.fetch('dis').success(function (value) {

          if (value === 0) {
            buttonData1 = {name: "red", image: button1, x: 320 - 32, y: 120, w: 32, h: 32};
            buttonData2 = {name: "green", image: button2, x: 0, y: 120, w: 32, h: 32};
          } else {
            buttonData2 = {name: "red", image: button1, x: 320 - 32, y: 120, w: 32, h: 32};
            buttonData1 = {name: "green", image: button2, x: 0, y: 120, w: 32, h: 32};
          }
          $scope.buttons.push(buttonData1);
          $scope.buttons.push(buttonData2);

        })
        .error(function (error) {
          alert("Error: " + error);
        });
    }

    function captureVideo() {
      if ($scope.abierto) {
        $scope.abierto = false;
        Animate.setVideoCanvas(document.getElementById('videoCanvas'));
        $state.go('tab.visitas');
      } else {
        $scope.abierto = true;
        Animate.setVideo($scope.video);
        Animate.setBlendCanvas(document.getElementById("blendCanvas"));
        Animate.setVideoCanvas(document.getElementById('videoCanvas'));
        Animate.setLayer2Canvas(document.getElementById('layer2'));
        Animate.setButtons($scope.buttons);
        UserMedia.start().then(sucessStartAnimation, function (e) {
          console.log("ANIMATION" + e);
        });
      }

    }

    function contabilizarNuevaVisita() {

      var cid = rfc4122.v4();
      if ($scope.analyticsID === null) {
        loadAnalytics();

      }

      var visitObj = {
        v: 1,
        tid: $scope.analyticsID,
        cid: cid,
        t: 'event',
        ec: $translate('EVENTO_CATEGORY'),
        ea: $translate('EVENTO_ACCION'),
        el: $translate('EVENTO_LABEL'),
        ev: 1,
        sc: 'start',
        ds: 'app'
      };

      databaseService.addVisit();
      $cordovaNativeAudio.play('click');
      if ($scope.analyticsID !== null) {
        VisitService.addVisit(visitObj);
        $timeout(destroyVisit, $scope.timer, true, cid);
      }
    }

    function destroyVisit(cid) {
      var destroyObject = {
        v: 1,
        tid: $scope.analyticsID,
        cid: cid,
        sc: 'stop'

      };
      VisitService.destroySession(destroyObject);
    }

    function animate() {
      if ($scope.abierto) {
        Animate.draw().then(drawSuccessFn, function () {
          newFrame();
        });
      }
    }


    function sucessStartAnimation(stream) {
      window.stream = stream; // stream available to console for dev
      if (window.URL) {
        $scope.videostream = $sce.trustAsResourceUrl(window.URL.createObjectURL(stream));
      } else {
        $scope.videostream = $sce.trustAsResourceUrl(stream);
      }
      animate();

    }

    function newFrame() {
      if ($scope.abierto) {
        ionic.requestAnimationFrame(animate);
      }
    }

    function drawSuccessFn(data) {
      if (data.status == 1) {

        Animate.blender().then(blenderSuccessFn, function () {
          console.log("error blender");
        });

      } else {
        newFrame();

      }

    }

    function blenderSuccessFn(data) {

      if (data.status == 1) {
        Animate.checkAreas().then(checkAreasSuccessFn, function () {
          console.log("error areas");
        });

      }
    }

    function checkAreasSuccessFn(data) {

      if (data.status === 1) {
        if (data.btn >= 0) {
          if (data.btn === 0) {
            $scope.inicio = true;
          } else {
            if (( $scope.inicio === true) && (data.btn == 1)) {
              $scope.inicio = false;
              contabilizarNuevaVisita();
            }
          }
        }
      }
      newFrame();
    }


  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('UserMedia', UserMedia);

  UserMedia.$inject = ['$q'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function UserMedia($q) {
    navigator.getUserMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);
    var constraints = {
      audio: false,
      video: {
        mandatory: {
          maxWidth: 320,
          maxHeight: 240
        }
      }
    };

    var deferred = $q.defer();
    var video = null;
    return {
      start: start,
      stop: stop
    };


    function start() {
      navigator.getUserMedia(
        constraints,
        function (stream) {
          video = stream;
          deferred.resolve(stream);
        },
        function errorCallback(error) {
          console.log('navigator.getUserMedia error: ', error);
          deferred.reject(error);
        }
      );

      return deferred.promise;

    }

    function stop() {
      var videoTrack = video.getVideoTracks();
      video.src='';
    }


  }
})();



/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('Animate', Animate);

  Animate.$inject = ['$q'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function Animate($q) {


    var buttons = null;
    var lastImageData = null;
    var video = null;
    var videoCanvas = null;
    var videoCtx = null;
    var layer2Canvas = null;
    var layer2Ctx = null;
    var blendCanvas = null;
    var blendCtx = null;
    return {
      setLayer2Canvas: setLayer2Canvas,
      setVideoCanvas: setVideoCanvas,
      setButtons: setButtons,
      setVideo: setVideo,
      setBlendCanvas: setBlendCanvas,
      draw: draw,
      blender: blender,
      checkAreas: checkAreas
    };


    function setBlendCanvas(obj) {

      blendCanvas = obj;
      blendCtx = blendCanvas.getContext('2d');

    }

    function setVideoCanvas(obj) {
      videoCanvas = obj;
      videoCtx = videoCanvas.getContext('2d');
      // these changes are permanent
      videoCtx.translate(320, 0);
      videoCtx.scale(-1, 1);

// background color if no video present
      videoCtx.fillStyle = '#008e94';
      videoCtx.fillRect(0, 0, videoCanvas.width, videoCanvas.height);
    }

    function setLayer2Canvas(obj) {

      layer2Canvas = obj;
      layer2Ctx = layer2Canvas.getContext('2d');
    }

    function setButtons(obj) {
      buttons = obj;
    }

    function setVideo(obj) {
      video = obj;
    }


    function draw() {
      var result = {status: 0};
      var deferred = $q.defer();
      if (video.readyState === video.HAVE_ENOUGH_DATA) {
        // mirror video
        videoCtx.drawImage(video, 0, 0, videoCanvas.width, videoCanvas.height);
        for (var i = 0; i < buttons.length; i++)
          layer2Ctx.drawImage(buttons[i].image, buttons[i].x, buttons[i].y, buttons[i].w, buttons[i].h);
        result.status = 1;
      }
      deferred.resolve(result);
      return deferred.promise;
    }

    function blender() {
      var result = {status: 0};
      var deferred = $q.defer();
      var width = videoCanvas.width;
      var height = videoCanvas.height;
      // get current webcam image data
      var sourceData = videoCtx.getImageData(0, 0, width, height);
      // create an image if the previous image doesn't exist
      if (!lastImageData) lastImageData = videoCtx.getImageData(0, 0, width, height);
      // create a ImageData instance to receive the blended result
      var blendedData = videoCtx.createImageData(width, height);
      // blend the 2 images
      differenceAccuracy(blendedData.data, sourceData.data, lastImageData.data);
      // draw the result in a canvas
      blendCtx.putImageData(blendedData, 0, 0);
      // store the current webcam image
      lastImageData = sourceData;
      result.status=1;
      deferred.resolve(result);
      return deferred.promise;
    }


    function checkAreas() {
      var result = {status: 0,btn:-1};

      var deferred = $q.defer();
      for (var b = 0; b < buttons.length; b++) {
        // get the pixels in a note area from the blended image
        var blendedData = blendCtx.getImageData(buttons[b].x, buttons[b].y, buttons[b].w, buttons[b].h);

        // calculate the average lightness of the blended data
        var i = 0;
        var sum = 0;
        var countPixels = blendedData.data.length * 0.25;
        while (i < countPixels) {
          sum += (blendedData.data[i * 4] + blendedData.data[i * 4 + 1] + blendedData.data[i * 4 + 2]);
          ++i;
        }
        // calculate an average between of the color values of the note area [0-255]
        var average = Math.round(sum / (3 * countPixels));

        if (average > 50) // more than 20% movement detected

        {
          result.btn = b;
          result.status = 1;
        }

      }

      deferred.resolve(result);
      return deferred.promise;


    }


    function differenceAccuracy(target, data1, data2) {
      if (data1.length != data2.length) return null;
      var i = 0;
      while (i < (data1.length * 0.25)) {
        var average1 = (data1[4 * i] + data1[4 * i + 1] + data1[4 * i + 2]) / 3;
        var average2 = (data2[4 * i] + data2[4 * i + 1] + data2[4 * i + 2]) / 3;
        var diff = threshold(fastAbs(average1 - average2));
        target[4 * i] = diff;
        target[4 * i + 1] = diff;
        target[4 * i + 2] = diff;
        target[4 * i + 3] = 0xFF;
        ++i;
      }
    }

    function fastAbs(value) {
      return (value ^ (value >> 31)) - (value >> 31);
    }

    function threshold(value) {
      return (value > 0x15) ? 0xFF : 0;
    }



  }
})();



/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('VisitService', Visit);

  Visit.$inject = ['$http'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function Visit($http) {

    return {
      addVisit: addVisit,
      destroySession: destroySession

    };


    function addVisit(content) {
      console.log("entro a añadir nueva visita a google");
      return sendPost(content);
    }

    function destroySession(content) {
      return sendPost(content);
    }

    function sendPost(content) {
      return $http({
        url: "http://www.google-analytics.com/collect",
        method: 'POST',
        params: content,
        paramSerializer: '$httpParamSerializerJQLike'
      });

    }


  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.settings.directives', []);
  angular
    .module('counterMax.settings.controllers', []);

  angular
    .module('counterMax.settings.services', []);
  angular
    .module('counterMax.settings.config', []);
  angular
    .module('counterMax.settings', [
      'counterMax.settings.controllers',
      'counterMax.settings.services',
      'counterMax.settings.config',
      'counterMax.settings.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.settings.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.settings.controllers')
    .controller('SettingsCtrl', SettingsCtrl);

  SettingsCtrl.$inject = ['$scope', '$cordovaPreferences', '$cordovaNativeAudio','$cordovaToast','$filter','$state','$cordovaGoogleAds','authService'];

  /**
   * @namespace AccountCtrl
   */
  function SettingsCtrl($scope, $cordovaPreferences, $cordovaNativeAudio,$cordovaToast,$filter,$state,$cordovaGoogleAds,authService) {

    $scope.settings = {};
    var $translate = $filter('translate');
    $scope.saveOptions = saveOptions;
    $scope.updateSound = updateSound;
    activate();


    function activate() {

      if (authService.isAuthenticated() === false) {
        $state.go('auth');

      } else {
        /**
         * Definimos los valores por defecto
         */
        $cordovaGoogleAds.showBanner(8);
        $cordovaPreferences.fetch('analyticsID').success(function (value) {
            $scope.settings.analyticsID = value;
          })
          .error(function (error) {
            alert("Error: " + error);
          });

        $cordovaPreferences.fetch('dis').success(function (value) {
            $scope.settings.disposition = value;
          })
          .error(function (error) {
            alert("Error: " + error);
          });


        $cordovaPreferences.fetch('sound')
          .success(function (value) {
            $scope.settings.sound = value;
          })
          .error(function (error) {
            $scope.settings.sound = 'sounds/Ariel.mp3';

          });

      }

    }

    function updateSound() {
      $cordovaNativeAudio.unload('click').then(successUnloadSound, failUnloadSound);
    }

    function saveOptions() {
      $cordovaPreferences.store('analyticsID', $scope.settings.analyticsID);
      $cordovaPreferences.store('dis', $scope.settings.disposition);
      $cordovaPreferences.store('sound', $scope.settings.sound);
      $cordovaToast.showLongBottom($translate('SETTINGS_SAVE')).then(function (success) {$state.go('tab.video');}, function (error) {

      });

    }

    function successUnloadSound(result) {

      $cordovaNativeAudio.preloadSimple('click', $scope.settings.sound).then(function (msg) {
        $cordovaNativeAudio.play('click');
      }, function (error) {
        alert(error);
      });


    }

    function failUnloadSound(err) {

      $cordovaNativeAudio.preloadSimple('click', $scope.settings.sound).then(function (msg) {
        $cordovaNativeAudio.play('click');
      }, function (error) {
        alert(error);
      });
    }


  }
})();

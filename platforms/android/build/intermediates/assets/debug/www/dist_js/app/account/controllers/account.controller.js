/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.account.controllers')
    .controller('AccountCtrl', AccountCtrl);

  AccountCtrl.$inject = ['$scope','$cordovaPreferences'];

  /**
   * @namespace AccountCtrl
   */
  function AccountCtrl($scope,$cordovaPreferences) {
    var vm = this;
    $scope.settings = {
      enableFriends: true
    };
    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.dashboard.controllers.DashCtrl
     */
    function activate() {
      $cordovaPreferences.show()
        .success(function(value) {
          alert("Success: " + value);
        })
        .error(function(error) {
          alert("Error: " + error);
        });
    }


  }
})();

/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.controllers')
    .controller('DefaultController', DefaultController);

  DefaultController.$inject = ['$scope', '$state','storageService','authService'];

  /**
   * @namespace AccountCtrl
   */
  function DefaultController($scope, $state,storageService,authService) {

    activate();
    function activate() {

      if (authService.isAuthenticated() === true) {
        var checkTutorial = storageService.get('checkTutorial', false);
        if (checkTutorial) {
          $state.go('tab.tutorial');

        } else {
          $state.go('tab.video');

        }


      } else {

        $state.go('auth');
      }
    }

  }
})();


/*

 .then(function(success) {
 window.localStorage.loginCorrect="true";
 if(window.localStorage.didTutorial === "true") {
 $state.go('tab.video');
 } else {

 $state.go('tab.tutorial');
 }



 }, function (error) {
 console.log(error);
 });




 function getAccessToken(userData) {
 alert("UserInfo: " + JSON.stringify(userData));
 facebookConnectPlugin.getAccessToken(function(token) {
 alert("Token: " + token);
 }, function(err) {
 alert("Could not get access token: " + err);
 });




 Oauth.convertFacebookToken(facebookToken,options)

 */

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('authService', authService);

  authService.$inject = ['storageService'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function authService(storageService) {

    return {

      isAuthenticated: isAuthenticated
    };

    function isAuthenticated() {

      var x = storageService.get('isRegistered', false);
      if (x == 'true') {

        return true;
      } else {
        return false;
      }
    }


  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('Animate', Animate);

  Animate.$inject = ['$q'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function Animate($q) {


    var buttons = null;
    var lastImageData = null;
    var video = null;
    var videoCanvas = null;
    var videoCtx = null;
    var layer2Canvas = null;
    var layer2Ctx = null;
    var blendCanvas = null;
    var blendCtx = null;
    return {
      setLayer2Canvas: setLayer2Canvas,
      setVideoCanvas: setVideoCanvas,
      setButtons: setButtons,
      setVideo: setVideo,
      setBlendCanvas: setBlendCanvas,
      draw: draw,
      blender: blender,
      checkAreas: checkAreas
    };


    function setBlendCanvas(obj) {

      blendCanvas = obj;
      blendCtx = blendCanvas.getContext('2d');

    }

    function setVideoCanvas(obj) {
      videoCanvas = obj;
      videoCtx = videoCanvas.getContext('2d');
      // these changes are permanent
      videoCtx.translate(320, 0);
      videoCtx.scale(-1, 1);

// background color if no video present
      videoCtx.fillStyle = '#008e94';
      videoCtx.fillRect(0, 0, videoCanvas.width, videoCanvas.height);
    }

    function setLayer2Canvas(obj) {

      layer2Canvas = obj;
      layer2Ctx = layer2Canvas.getContext('2d');
    }

    function setButtons(obj) {
      buttons = obj;
    }

    function setVideo(obj) {
      video = obj;
    }


    function draw() {
      var result = {status: 0};
      var deferred = $q.defer();
      if (video.readyState === video.HAVE_ENOUGH_DATA) {
        // mirror video
        videoCtx.drawImage(video, 0, 0, videoCanvas.width, videoCanvas.height);
        for (var i = 0; i < buttons.length; i++)
          layer2Ctx.drawImage(buttons[i].image, buttons[i].x, buttons[i].y, buttons[i].w, buttons[i].h);
        result.status = 1;
      }
      deferred.resolve(result);
      return deferred.promise;
    }

    function blender() {
      var result = {status: 0};
      var deferred = $q.defer();
      var width = videoCanvas.width;
      var height = videoCanvas.height;
      // get current webcam image data
      var sourceData = videoCtx.getImageData(0, 0, width, height);
      // create an image if the previous image doesn't exist
      if (!lastImageData) lastImageData = videoCtx.getImageData(0, 0, width, height);
      // create a ImageData instance to receive the blended result
      var blendedData = videoCtx.createImageData(width, height);
      // blend the 2 images
      differenceAccuracy(blendedData.data, sourceData.data, lastImageData.data);
      // draw the result in a canvas
      blendCtx.putImageData(blendedData, 0, 0);
      // store the current webcam image
      lastImageData = sourceData;
      result.status=1;
      deferred.resolve(result);
      return deferred.promise;
    }


    function checkAreas() {
      var result = {status: 0,btn:-1};

      var deferred = $q.defer();
      for (var b = 0; b < buttons.length; b++) {
        // get the pixels in a note area from the blended image
        var blendedData = blendCtx.getImageData(buttons[b].x, buttons[b].y, buttons[b].w, buttons[b].h);

        // calculate the average lightness of the blended data
        var i = 0;
        var sum = 0;
        var countPixels = blendedData.data.length * 0.25;
        while (i < countPixels) {
          sum += (blendedData.data[i * 4] + blendedData.data[i * 4 + 1] + blendedData.data[i * 4 + 2]);
          ++i;
        }
        // calculate an average between of the color values of the note area [0-255]
        var average = Math.round(sum / (3 * countPixels));

        if (average > 50) // more than 20% movement detected

        {
          result.btn = b;
          result.status = 1;
        }

      }

      deferred.resolve(result);
      return deferred.promise;


    }


    function differenceAccuracy(target, data1, data2) {
      if (data1.length != data2.length) return null;
      var i = 0;
      while (i < (data1.length * 0.25)) {
        var average1 = (data1[4 * i] + data1[4 * i + 1] + data1[4 * i + 2]) / 3;
        var average2 = (data2[4 * i] + data2[4 * i + 1] + data2[4 * i + 2]) / 3;
        var diff = threshold(fastAbs(average1 - average2));
        target[4 * i] = diff;
        target[4 * i + 1] = diff;
        target[4 * i + 2] = diff;
        target[4 * i + 3] = 0xFF;
        ++i;
      }
    }

    function fastAbs(value) {
      return (value ^ (value >> 31)) - (value >> 31);
    }

    function threshold(value) {
      return (value > 0x15) ? 0xFF : 0;
    }



  }
})();



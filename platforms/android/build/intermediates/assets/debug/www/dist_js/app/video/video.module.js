(function () {
  'use strict';
  angular
    .module('counterMax.video.directives', []);
  angular
    .module('counterMax.video.controllers', []);

  angular
    .module('counterMax.video.services', []);
  angular
    .module('counterMax.video.config', []);
  angular
    .module('counterMax.video', [
      'counterMax.video.controllers',
      'counterMax.video.services',
      'counterMax.video.config',
      'counterMax.video.directives',
      'uuid'
    ]);

})();

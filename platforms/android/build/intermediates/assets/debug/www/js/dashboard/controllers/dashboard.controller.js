/**
 * DashCtrl
 * @namespace counterMax.dashboard.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.dashboard.controllers')
    .controller('DashCtrl', DashCtrl);

  DashCtrl.$inject = ['$scope','$cordovaGoogleAds','$state','authService'];

  /**
   * @namespace HotelBookingController
   */
  function DashCtrl($scope,$cordovaGoogleAds,$state,authService) {

    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.dashboard.controllers.DashCtrl
     */
    function activate() {

      if (authService.isAuthenticated() === false) {
        $state.go('auth');

      } else {
        $cordovaGoogleAds.showBanner(8);
      }

    }



  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('endpointsService', endpointsService);

  endpointsService.$inject = ['$http', '$state', 'storageService'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function endpointsService($http, $state, storageService) {
    var config = {
      baseUrl: 'https://www.countermax.com',
      clientId: 'fnu10PTJnT8EJCYuRKMecawwJH6B8gVKAEFi48Do',
      clientSecret: null,
      convertTokenPath: '/auth/convert-token',
      grantPath: '/auth/token',
      revokePath: '/auth/revoke-token',
      loginUrl: '/account/login'

    };
    var globalUserObject;

    return {

      convertFacebookToken: convertFacebookToken,
      loginRegister: loginRegister
    };

    function convertFacebookToken(token) {

      var content = {
        client_id: config.clientId,
        grant_type: 'convert_token',
        backend: 'facebook',
        token: token
      };


      $http({
        url: config.baseUrl + config.convertTokenPath,
        method: 'POST',
        params: content,
        paramSerializer: '$httpParamSerializerJQLike'
      }).then(successTokenCallback, errorTokenCallback);
    }


    function successTokenCallback(response) {
      storageService.setObject('token', response);
      storageService.set('isRegistered', true);
      storageService.set('checkTutorial', true);
      $state.go('tab.tutorial');

      return true;
    }

    function errorTokenCallback(response) {
      console.log(response);
      return false;
    }


    function loginRegister(userObject) {
      globalUserObject = userObject;
      var content = {
        email: userObject.email,
        password: userObject.password
      };
      $http.post(config.baseUrl + config.loginUrl, content).then(accountToken, function (data, status, headers, config, userObject) {
          console.log(data);
        }
      );


    }

    function accountToken() {
      var content = {
        client_id: config.clientId,
        grant_type: 'password',
        username: globalUserObject.email,
        password: globalUserObject.password
      };

      $http({
        url: config.baseUrl + config.grantPath,
        method: 'POST',
        params: content,
        paramSerializer: '$httpParamSerializerJQLike'
      }).then(successTokenCallback, errorTokenCallback);
    }


  }
})();

-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:2:100
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:2:71
	android:hardwareAccelerated
		ADDED from AndroidManifest.xml:2:11
	android:versionCode
		ADDED from AndroidManifest.xml:2:46
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:140
supports-screens
ADDED from AndroidManifest.xml:3:5
	android:largeScreens
		ADDED from AndroidManifest.xml:3:49
	android:smallScreens
		ADDED from AndroidManifest.xml:3:132
	android:normalScreens
		ADDED from AndroidManifest.xml:3:77
	android:xlargeScreens
		ADDED from AndroidManifest.xml:3:160
	android:resizeable
		ADDED from AndroidManifest.xml:3:106
	android:anyDensity
		ADDED from AndroidManifest.xml:3:23
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:4:5
MERGED from com.google.android.gms:play-services-analytics:8.4.0:21:5
MERGED from com.google.android.gms:play-services-gcm:8.4.0:22:5
MERGED from com.google.android.gms:play-services-measurement:8.4.0:21:5
MERGED from com.google.android.gms:play-services-ads:8.4.0:20:5
	android:name
		ADDED from AndroidManifest.xml:4:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:5:5
MERGED from com.google.android.gms:play-services-analytics:8.4.0:22:5
MERGED from com.google.android.gms:play-services-measurement:8.4.0:22:5
MERGED from com.google.android.gms:play-services-ads:8.4.0:21:5
	android:name
		ADDED from AndroidManifest.xml:5:22
uses-permission#android.permission.ACCESS_WIFI_STATE
ADDED from AndroidManifest.xml:6:5
	android:name
		ADDED from AndroidManifest.xml:6:22
application
ADDED from AndroidManifest.xml:7:5
MERGED from com.google.android.gms:play-services-analytics:8.4.0:24:5
MERGED from com.google.android.gms:play-services-basement:8.4.0:20:5
MERGED from com.android.support:support-v4:23.1.1:22:5
MERGED from com.android.support:support-v13:23.1.1:22:5
MERGED from com.android.support:support-v4:23.1.1:22:5
MERGED from com.google.android.gms:play-services-gcm:8.4.0:24:5
MERGED from com.google.android.gms:play-services-basement:8.4.0:20:5
MERGED from com.android.support:support-v4:23.1.1:22:5
MERGED from com.google.android.gms:play-services-measurement:8.4.0:26:5
MERGED from com.google.android.gms:play-services-basement:8.4.0:20:5
MERGED from com.android.support:support-v4:23.1.1:22:5
MERGED from com.android.support:support-v4:23.1.1:22:5
MERGED from com.facebook.android:facebook-android-sdk:3.23.0:24:5
MERGED from com.android.support:support-v4:23.1.1:22:5
MERGED from com.google.android.gms:play-services-ads:8.4.0:24:5
MERGED from com.google.android.gms:play-services-basement:8.4.0:20:5
MERGED from com.android.support:support-v4:23.1.1:22:5
	android:label
		ADDED from AndroidManifest.xml:7:83
	android:supportsRtl
		ADDED from AndroidManifest.xml:7:116
	android:hardwareAccelerated
		ADDED from AndroidManifest.xml:7:18
	android:icon
		ADDED from AndroidManifest.xml:7:53
activity#com.teamrockmotion.counterMax.MainActivity
ADDED from AndroidManifest.xml:8:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:8:194
	android:label
		ADDED from AndroidManifest.xml:8:97
	android:launchMode
		ADDED from AndroidManifest.xml:8:135
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:8:285
	android:configChanges
		ADDED from AndroidManifest.xml:8:19
	android:theme
		ADDED from AndroidManifest.xml:8:231
	android:name
		ADDED from AndroidManifest.xml:8:166
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:9:13
	android:label
		ADDED from AndroidManifest.xml:9:28
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:10:17
	android:name
		ADDED from AndroidManifest.xml:10:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:11:17
	android:name
		ADDED from AndroidManifest.xml:11:27
receiver#com.google.android.gms.analytics.AnalyticsReceiver
ADDED from AndroidManifest.xml:14:9
	android:enabled
		ADDED from AndroidManifest.xml:14:19
	android:name
		ADDED from AndroidManifest.xml:14:42
intent-filter#com.google.android.gms.analytics.ANALYTICS_DISPATCH
ADDED from AndroidManifest.xml:15:13
action#com.google.android.gms.analytics.ANALYTICS_DISPATCH
ADDED from AndroidManifest.xml:16:17
	android:name
		ADDED from AndroidManifest.xml:16:25
service#com.google.android.gms.analytics.AnalyticsService
ADDED from AndroidManifest.xml:19:9
	android:enabled
		ADDED from AndroidManifest.xml:19:18
	android:exported
		ADDED from AndroidManifest.xml:19:41
	android:name
		ADDED from AndroidManifest.xml:19:66
receiver#com.google.android.gms.analytics.CampaignTrackingReceiver
ADDED from AndroidManifest.xml:20:9
	android:exported
		ADDED from AndroidManifest.xml:20:19
	android:name
		ADDED from AndroidManifest.xml:20:43
intent-filter#com.android.vending.INSTALL_REFERRER
ADDED from AndroidManifest.xml:21:13
action#com.android.vending.INSTALL_REFERRER
ADDED from AndroidManifest.xml:22:17
	android:name
		ADDED from AndroidManifest.xml:22:25
service#com.google.android.gms.analytics.CampaignTrackingService
ADDED from AndroidManifest.xml:25:9
	android:name
		ADDED from AndroidManifest.xml:25:18
activity#com.adobe.phonegap.push.PushHandlerActivity
ADDED from AndroidManifest.xml:26:9
	android:exported
		ADDED from AndroidManifest.xml:26:19
	android:name
		ADDED from AndroidManifest.xml:26:43
receiver#com.google.android.gms.gcm.GcmReceiver
ADDED from AndroidManifest.xml:27:9
	android:exported
		ADDED from AndroidManifest.xml:27:19
	android:permission
		ADDED from AndroidManifest.xml:27:97
	android:name
		ADDED from AndroidManifest.xml:27:43
intent-filter#${applicationId}+com.google.android.c2dm.intent.RECEIVE
ADDED from AndroidManifest.xml:28:13
action#com.google.android.c2dm.intent.RECEIVE
ADDED from AndroidManifest.xml:29:17
	android:name
		ADDED from AndroidManifest.xml:29:25
category#${applicationId}
ADDED from AndroidManifest.xml:30:17
	android:name
		ADDED from AndroidManifest.xml:30:27
		INJECTED from AndroidManifest.xml:0:0
service#com.adobe.phonegap.push.GCMIntentService
ADDED from AndroidManifest.xml:33:9
	android:exported
		ADDED from AndroidManifest.xml:33:18
	android:name
		ADDED from AndroidManifest.xml:33:43
intent-filter#com.google.android.c2dm.intent.RECEIVE
ADDED from AndroidManifest.xml:34:13
service#com.adobe.phonegap.push.PushInstanceIDListenerService
ADDED from AndroidManifest.xml:38:9
	android:exported
		ADDED from AndroidManifest.xml:38:18
	android:name
		ADDED from AndroidManifest.xml:38:43
intent-filter#com.google.android.gms.iid.InstanceID
ADDED from AndroidManifest.xml:39:13
action#com.google.android.gms.iid.InstanceID
ADDED from AndroidManifest.xml:40:17
	android:name
		ADDED from AndroidManifest.xml:40:25
service#com.adobe.phonegap.push.RegistrationIntentService
ADDED from AndroidManifest.xml:43:9
	android:exported
		ADDED from AndroidManifest.xml:43:18
	android:name
		ADDED from AndroidManifest.xml:43:43
activity#me.apla.cordova.AppPreferencesActivity
ADDED from AndroidManifest.xml:44:9
	android:name
		ADDED from AndroidManifest.xml:44:19
meta-data#com.facebook.sdk.ApplicationId
ADDED from AndroidManifest.xml:45:9
	android:value
		ADDED from AndroidManifest.xml:45:66
	android:name
		ADDED from AndroidManifest.xml:45:20
activity#com.facebook.LoginActivity
ADDED from AndroidManifest.xml:46:9
	android:label
		ADDED from AndroidManifest.xml:46:19
	android:theme
		ADDED from AndroidManifest.xml:46:97
	android:name
		ADDED from AndroidManifest.xml:46:55
activity#com.google.android.gms.ads.AdActivity
ADDED from AndroidManifest.xml:47:9
MERGED from com.google.android.gms:play-services-ads:8.4.0:26:9
	android:configChanges
		ADDED from AndroidManifest.xml:47:19
	android:theme
		ADDED from AndroidManifest.xml:47:182
	android:name
		ADDED from AndroidManifest.xml:47:129
uses-sdk
ADDED from AndroidManifest.xml:49:5
MERGED from com.google.android.gms:play-services-analytics:8.4.0:18:5
MERGED from com.google.android.gms:play-services-basement:8.4.0:18:5
MERGED from com.android.support:support-v4:23.1.1:20:5
MERGED from com.android.support:support-v13:23.1.1:20:5
MERGED from com.android.support:support-v4:23.1.1:20:5
MERGED from com.google.android.gms:play-services-gcm:8.4.0:18:5
MERGED from com.google.android.gms:play-services-base:8.4.0:18:5
MERGED from com.google.android.gms:play-services-basement:8.4.0:18:5
MERGED from com.android.support:support-v4:23.1.1:20:5
MERGED from com.google.android.gms:play-services-measurement:8.4.0:18:5
MERGED from com.google.android.gms:play-services-basement:8.4.0:18:5
MERGED from com.android.support:support-v4:23.1.1:20:5
MERGED from com.android.support:support-v4:23.1.1:20:5
MERGED from com.facebook.android:facebook-android-sdk:3.23.0:20:5
MERGED from com.android.support:support-v4:23.1.1:20:5
MERGED from com.google.android.gms:play-services-ads:8.4.0:23:5
MERGED from com.google.android.gms:play-services-basement:8.4.0:18:5
MERGED from com.android.support:support-v4:23.1.1:20:5
MERGED from org.xwalk:xwalk_core_library:15.44.384.13:6:5
MERGED from android:CordovaLib:unspecified:debug:25:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:49:42
	android:minSdkVersion
		ADDED from AndroidManifest.xml:49:15
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:50:5
	android:name
		ADDED from AndroidManifest.xml:50:22
uses-permission#android.permission.RECORD_AUDIO
ADDED from AndroidManifest.xml:51:5
	android:name
		ADDED from AndroidManifest.xml:51:22
uses-permission#android.permission.MODIFY_AUDIO_SETTINGS
ADDED from AndroidManifest.xml:52:5
	android:name
		ADDED from AndroidManifest.xml:52:22
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:53:5
	android:name
		ADDED from AndroidManifest.xml:53:22
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:54:5
MERGED from com.google.android.gms:play-services-measurement:8.4.0:24:5
	android:name
		ADDED from AndroidManifest.xml:54:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:55:5
	android:name
		ADDED from AndroidManifest.xml:55:22
uses-permission#android.permission.CAMERA
ADDED from AndroidManifest.xml:56:5
	android:name
		ADDED from AndroidManifest.xml:56:22
uses-feature#android.hardware.camera
ADDED from AndroidManifest.xml:57:5
	android:name
		ADDED from AndroidManifest.xml:57:19
uses-feature#android.hardware.camera.autofocus
ADDED from AndroidManifest.xml:58:5
	android:name
		ADDED from AndroidManifest.xml:58:19
uses-feature#0x00020000
ADDED from AndroidManifest.xml:59:5
	android:glEsVersion
		ADDED from AndroidManifest.xml:59:19
	android:required
		ADDED from AndroidManifest.xml:59:52
uses-permission#com.google.android.c2dm.permission.RECEIVE
ADDED from AndroidManifest.xml:60:5
MERGED from com.google.android.gms:play-services-gcm:8.4.0:21:5
	android:name
		ADDED from AndroidManifest.xml:60:22
permission#${applicationId}.permission.C2D_MESSAGE
ADDED from AndroidManifest.xml:61:5
	android:protectionLevel
		ADDED from AndroidManifest.xml:61:72
	android:name
		ADDED from AndroidManifest.xml:61:17
		INJECTED from AndroidManifest.xml:0:0
uses-permission#${applicationId}.permission.C2D_MESSAGE
ADDED from AndroidManifest.xml:62:5
	android:name
		ADDED from AndroidManifest.xml:62:22
		INJECTED from AndroidManifest.xml:0:0
meta-data#com.google.android.gms.version
ADDED from com.google.android.gms:play-services-basement:8.4.0:21:9
MERGED from com.google.android.gms:play-services-basement:8.4.0:21:9
MERGED from com.google.android.gms:play-services-basement:8.4.0:21:9
MERGED from com.google.android.gms:play-services-basement:8.4.0:21:9
	android:value
		ADDED from com.google.android.gms:play-services-basement:8.4.0:23:13
	android:name
		ADDED from com.google.android.gms:play-services-basement:8.4.0:22:13
provider#com.google.android.gms.measurement.AppMeasurementContentProvider
ADDED from com.google.android.gms:play-services-measurement:8.4.0:28:7
	android:authorities
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:29:11
	android:exported
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:31:11
	android:name
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:30:11
receiver#com.google.android.gms.measurement.AppMeasurementReceiver
ADDED from com.google.android.gms:play-services-measurement:8.4.0:33:7
	android:enabled
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:35:11
	android:name
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:34:11
intent-filter#com.google.android.gms.measurement.UPLOAD
ADDED from com.google.android.gms:play-services-measurement:8.4.0:36:9
action#com.google.android.gms.measurement.UPLOAD
ADDED from com.google.android.gms:play-services-measurement:8.4.0:37:11
	android:name
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:37:19
service#com.google.android.gms.measurement.AppMeasurementService
ADDED from com.google.android.gms:play-services-measurement:8.4.0:41:7
	android:enabled
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:43:11
	android:exported
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:44:11
	android:name
		ADDED from com.google.android.gms:play-services-measurement:8.4.0:42:11
activity#com.google.android.gms.ads.purchase.InAppPurchaseActivity
ADDED from com.google.android.gms:play-services-ads:8.4.0:30:9
	android:theme
		ADDED from com.google.android.gms:play-services-ads:8.4.0:31:13
	android:name
		ADDED from com.google.android.gms:play-services-ads:8.4.0:30:19

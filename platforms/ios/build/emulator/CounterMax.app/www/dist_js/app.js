/*
 AngularJS v1.4.8
 (c) 2010-2015 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(p,c,n){'use strict';function l(b,a,g){var d=g.baseHref(),k=b[0];return function(b,e,f){var g,h;f=f||{};h=f.expires;g=c.isDefined(f.path)?f.path:d;c.isUndefined(e)&&(h="Thu, 01 Jan 1970 00:00:00 GMT",e="");c.isString(h)&&(h=new Date(h));e=encodeURIComponent(b)+"="+encodeURIComponent(e);e=e+(g?";path="+g:"")+(f.domain?";domain="+f.domain:"");e+=h?";expires="+h.toUTCString():"";e+=f.secure?";secure":"";f=e.length+1;4096<f&&a.warn("Cookie '"+b+"' possibly not set or overflowed because it was too large ("+
f+" > 4096 bytes)!");k.cookie=e}}c.module("ngCookies",["ng"]).provider("$cookies",[function(){var b=this.defaults={};this.$get=["$$cookieReader","$$cookieWriter",function(a,g){return{get:function(d){return a()[d]},getObject:function(d){return(d=this.get(d))?c.fromJson(d):d},getAll:function(){return a()},put:function(d,a,m){g(d,a,m?c.extend({},b,m):b)},putObject:function(d,b,a){this.put(d,c.toJson(b),a)},remove:function(a,k){g(a,n,k?c.extend({},b,k):b)}}}]}]);c.module("ngCookies").factory("$cookieStore",
["$cookies",function(b){return{get:function(a){return b.getObject(a)},put:function(a,c){b.putObject(a,c)},remove:function(a){b.remove(a)}}}]);l.$inject=["$document","$log","$browser"];c.module("ngCookies").provider("$$cookieWriter",function(){this.$get=l})})(window,window.angular);
//# sourceMappingURL=angular-cookies.min.js.map

cordova.define("com.phonegap.plugins.facebookconnect.FacebookConnectPlugin", function(require, exports, module) {

    "use strict";

    /*
     * @author Ally Ogilvie
     * @copyright Wizcorp Inc. [ Incorporated Wizards ] 2014
     * @file - facebookConnectPlugin.js
     * @about - JavaScript interface for PhoneGap bridge to Facebook Connect SDK
     *
     *
     */

    if (cordova.platformId == "browser") {

        var facebookConnectPlugin = {

            getLoginStatus: function (s, f) {
                // Try will catch errors when SDK has not been init
                try {
                    FB.getLoginStatus(function (response) {
                        s(response);
                    });
                } catch (error) {
                    if (!f) {
                        console.error(error.message);
                    } else {
                        f(error.message);
                    }
                }
            },

            showDialog: function (options, s, f) {

                if (!options.name) {
                    options.name = "";
                }
                if (!options.message) {
                    options.message = "";
                }
                if (!options.caption) {
                    options.caption = "";
                }
                if (!options.description) {
                    options.description = "";
                }
                if (!options.href) {
                    options.href = "";
                }
                if (!options.picture) {
                    options.picture = "";
                }
                
                // Try will catch errors when SDK has not been init
                try {
                    FB.ui(options,
                    function (response) {
                        if (response && (response.request || !response.error_code)) {
                            s(response);
                        } else {
                            f(response);
                        }
                    });
                } catch (error) {
                    if (!f) {
                        console.error(error.message);
                    } else {
                        f(error.message);
                    }
                }
            },
            // Attach this to a UI element, this requires user interaction.
            login: function (permissions, s, f) {
                // JS SDK takes an object here but the native SDKs use array.
                var permissionObj = {};
                if (permissions && permissions.length > 0) {
                    permissionObj.scope = permissions.toString();
                }
                
                FB.login(function (response) {
                    if (response.authResponse) {
                        s(response);
                    } else {
                        f(response.status);
                    }
                }, permissionObj);
            },

            getAccessToken: function (s, f) {
                var response = FB.getAccessToken();
                if (!response) {
                    if (!f) {
                        console.error("NO_TOKEN");
                    } else {
                        f("NO_TOKEN");
                    }
                } else {
                    s(response);
                }
            },

            logEvent: function (eventName, params, valueToSum, s, f) {
                // AppEvents are not avaliable in JS.
                s();
            },

            logPurchase: function (value, currency, s, f) {
                // AppEvents are not avaliable in JS.
                s();
            },

            logout: function (s, f) {
                // Try will catch errors when SDK has not been init
                try {
                    FB.logout( function (response) {
                        s(response);
                    });
                } catch (error) {
                    if (!f) {
                        console.error(error.message);
                    } else {
                        f(error.message);
                    }
                }
            },

            api: function (graphPath, permissions, s, f) {
                // JS API does not take additional permissions
                
                // Try will catch errors when SDK has not been init
                try {
                    FB.api(graphPath, function (response) {
                        if (response.error) {
                            f(response);
                        } else {
                            s(response);
                        }
                    });
                } catch (error) {
                    if (!f) {
                        console.error(error.message);
                    } else {
                        f(error.message);
                    }
                }
            },

            // Browser wrapper API ONLY
            browserInit: function (appId, version) {
                if (!version) {
                    version = "v2.0";
                }
                FB.init({
                    appId      : appId,
                    cookie     : true,
                    xfbml      : true,
                    version    : version
                });
            }
        };
        
        // Bake in the JS SDK
        (function () {
            if (!window.FB) {
                console.log("launching FB SDK");
                var e = document.createElement('script');
                e.src = document.location.protocol + '//connect.facebook.net/en_US/sdk.js';
                e.async = true;
                document.getElementById('fb-root').appendChild(e);
                if (!window.FB) {
                    // Probably not on server, use the sample sdk
                    e.src = 'phonegap/plugin/facebookConnectPlugin/fbsdk.js';
                    document.getElementById('fb-root').appendChild(e);
                    console.log("Attempt local load: ", e);
                }
            }
        }());

        module.exports = facebookConnectPlugin;

    } else {

        var exec = require("cordova/exec");

        var facebookConnectPlugin = {

            getLoginStatus: function (s, f) {
                exec(s, f, "FacebookConnectPlugin", "getLoginStatus", []);
            },

            showDialog: function (options, s, f) {
                exec(s, f, "FacebookConnectPlugin", "showDialog", [options]);
            },

            login: function (permissions, s, f) {
                exec(s, f, "FacebookConnectPlugin", "login", permissions);
            },

            logEvent: function(name, params, valueToSum, s, f) {
                // Prevent NSNulls getting into iOS, messes up our [command.argument count]
                if (!params && !valueToSum) {
                    exec(s, f, "FacebookConnectPlugin", "logEvent", [name]);
                } else if (params && !valueToSum) {
                    exec(s, f, "FacebookConnectPlugin", "logEvent", [name, params]);
                } else if (params && valueToSum) {
                    exec(s, f, "FacebookConnectPlugin", "logEvent", [name, params, valueToSum]);
                } else {
                    f("Invalid arguments");
                }
            },

            logPurchase: function(value, currency, s, f) {
                exec(s, f, "FacebookConnectPlugin", "logPurchase", [value, currency]);
            },

            getAccessToken: function(s, f) {
                exec(s, f, "FacebookConnectPlugin", "getAccessToken", []);
            },

            logout: function (s, f) {
                exec(s, f, "FacebookConnectPlugin", "logout", []);
            },

            api: function (graphPath, permissions, s, f) {
                if (!permissions) { permissions = []; }
                exec(s, f, "FacebookConnectPlugin", "graphApi", [graphPath, permissions]);
            }
        };

        module.exports = facebookConnectPlugin;
    }
});
/*!
	query-string
	Parse and stringify URL query strings
	https://github.com/sindresorhus/query-string
	by Sindre Sorhus
	MIT License
*/
(function () {
	'use strict';
	var queryString = {};

	queryString.parse = function (str) {
		if (typeof str !== 'string') {
			return {};
		}

		str = str.trim().replace(/^(\?|#)/, '');

		if (!str) {
			return {};
		}

		return str.trim().split('&').reduce(function (ret, param) {
			var parts = param.replace(/\+/g, ' ').split('=');
			var key = parts[0];
			var val = parts[1];

			key = decodeURIComponent(key);
			// missing `=` should be `null`:
			// http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
			val = val === undefined ? null : decodeURIComponent(val);

			if (!ret.hasOwnProperty(key)) {
				ret[key] = val;
			} else if (Array.isArray(ret[key])) {
				ret[key].push(val);
			} else {
				ret[key] = [ret[key], val];
			}

			return ret;
		}, {});
	};

	queryString.stringify = function (obj) {
		return obj ? Object.keys(obj).map(function (key) {
			var val = obj[key];

			if (Array.isArray(val)) {
				return val.map(function (val2) {
					return encodeURIComponent(key) + '=' + encodeURIComponent(val2);
				}).join('&');
			}

			return encodeURIComponent(key) + '=' + encodeURIComponent(val);
		}).join('&') : '';
	};

	if (typeof define === 'function' && define.amd) {
		define(function() { return queryString; });
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = queryString;
	} else {
		self.queryString = queryString;
	}
})();

!function(e,t){"function"==typeof define&&define.amd?define(["angular","query-string"],t):"object"==typeof exports?module.exports=t(require("angular"),require("query-string")):e.angularOAuth2=t(e.angular,e.queryString)}(this,function(e,t){function r(e){e.interceptors.push("oauthInterceptor")}function n(){var r;this.configure=function(t){if(r)throw new Error("Already configured.");if(!(t instanceof Object))throw new TypeError("Invalid argument: `config` must be an `Object`.");return r=e.extend({},c,t),e.forEach(s,function(e){if(!r[e])throw new Error("Missing parameter: "+e+".")}),"/"===r.baseUrl.substr(-1)&&(r.baseUrl=r.baseUrl.slice(0,-1)),"/"!==r.grantPath[0]&&(r.grantPath="/"+r.grantPath),"/"!==r.revokePath[0]&&(r.revokePath="/"+r.revokePath),r},this.$get=function(n,o){var a=function(){function a(){if(!r)throw new Error("`OAuthProvider` must be configured first.")}return u(a,null,{isAuthenticated:{value:function(){return!!o.getToken()},writable:!0,enumerable:!0,configurable:!0},getAccessToken:{value:function(a,i){if(!a||!a.username||!a.password)throw new Error("`user` must be an object with `username` and `password` properties.");var u={client_id:r.clientId,grant_type:"password",username:a.username,password:a.password};return null!==r.clientSecret&&(u.client_secret=r.clientSecret),u=t.stringify(u),i=e.extend({headers:{"Content-Type":"application/x-www-form-urlencoded"}},i),n.post(""+r.baseUrl+r.grantPath,u,i).then(function(e){return o.setToken(e.data),e})},writable:!0,enumerable:!0,configurable:!0},getRefreshToken:{value:function(){var e={client_id:r.clientId,grant_type:"refresh_token",refresh_token:o.getRefreshToken()};null!==r.clientSecret&&(e.client_secret=r.clientSecret),e=t.stringify(e);var a={headers:{"Content-Type":"application/x-www-form-urlencoded"}};return n.post(""+r.baseUrl+r.grantPath,e,a).then(function(e){return o.setToken(e.data),e})},writable:!0,enumerable:!0,configurable:!0},revokeToken:{value:function(){var e=t.stringify({token:o.getRefreshToken()?o.getRefreshToken():o.getAccessToken()}),a={headers:{"Content-Type":"application/x-www-form-urlencoded"}};return n.post(""+r.baseUrl+r.revokePath,e,a).then(function(e){return o.removeToken(),e})},writable:!0,enumerable:!0,configurable:!0}}),a}();return new a},this.$get.$inject=["$http","OAuthToken"]}function o(){var t={name:"token",options:{secure:!0}};this.configure=function(r){if(!(r instanceof Object))throw new TypeError("Invalid argument: `config` must be an `Object`.");return e.extend(t,r),t},this.$get=function(e){var r=function(){function r(){}return u(r,null,{setToken:{value:function(r){return e.putObject(t.name,r,t.options)},writable:!0,enumerable:!0,configurable:!0},getToken:{value:function(){return e.getObject(t.name)},writable:!0,enumerable:!0,configurable:!0},getAccessToken:{value:function(){return this.getToken()?this.getToken().access_token:void 0},writable:!0,enumerable:!0,configurable:!0},getAuthorizationHeader:{value:function(){return this.getTokenType()&&this.getAccessToken()?""+(this.getTokenType().charAt(0).toUpperCase()+this.getTokenType().substr(1))+" "+this.getAccessToken():void 0},writable:!0,enumerable:!0,configurable:!0},getRefreshToken:{value:function(){return this.getToken()?this.getToken().refresh_token:void 0},writable:!0,enumerable:!0,configurable:!0},getTokenType:{value:function(){return this.getToken()?this.getToken().token_type:void 0},writable:!0,enumerable:!0,configurable:!0},removeToken:{value:function(){return e.remove(t.name,t.options)},writable:!0,enumerable:!0,configurable:!0}}),r}();return new r},this.$get.$inject=["$cookies"]}function a(e,t,r){return{request:function(e){return r.getAuthorizationHeader()&&(e.headers=e.headers||{},e.headers.Authorization=r.getAuthorizationHeader()),e},responseError:function(n){return 400!==n.status||!n.data||"invalid_request"!==n.data.error&&"invalid_grant"!==n.data.error||(r.removeToken(),t.$emit("oauth:error",n)),(401===n.status&&n.data&&"invalid_token"===n.data.error||n.headers("www-authenticate")&&0===n.headers("www-authenticate").indexOf("Bearer"))&&t.$emit("oauth:error",n),e.reject(n)}}}var i=e.module("angular-oauth2",["ngCookies"]).config(r).factory("oauthInterceptor",a).provider("OAuth",n).provider("OAuthToken",o);r.$inject=["$httpProvider"];var u=function(e,t,r){t&&Object.defineProperties(e,t),r&&Object.defineProperties(e.prototype,r)},c={baseUrl:null,clientId:null,clientSecret:null,grantPath:"/oauth2/token",revokePath:"/oauth2/revoke"},s=["baseUrl","clientId","grantPath","revokePath"],u=function(e,t,r){t&&Object.defineProperties(e,t),r&&Object.defineProperties(e.prototype,r)};return a.$inject=["$q","$rootScope","OAuthToken"],i});
/*!
 * angular-translate - v2.8.1 - 2015-10-01
 * 
 * Copyright (c) 2015 The angular-translate team, Pascal Precht; Licensed MIT
 */
!function(a,b){"function"==typeof define&&define.amd?define([],function(){return b()}):"object"==typeof exports?module.exports=b():b()}(this,function(){function a(a){"use strict";var b=a.storageKey(),c=a.storage(),d=function(){var d=a.preferredLanguage();angular.isString(d)?a.use(d):c.put(b,a.use())};d.displayName="fallbackFromIncorrectStorageValue",c?c.get(b)?a.use(c.get(b))["catch"](d):d():angular.isString(a.preferredLanguage())&&a.use(a.preferredLanguage())}function b(){"use strict";var a,b,c=null,d=!1,e=!1;b={sanitize:function(a,b){return"text"===b&&(a=g(a)),a},escape:function(a,b){return"text"===b&&(a=f(a)),a},sanitizeParameters:function(a,b){return"params"===b&&(a=h(a,g)),a},escapeParameters:function(a,b){return"params"===b&&(a=h(a,f)),a}},b.escaped=b.escapeParameters,this.addStrategy=function(a,c){return b[a]=c,this},this.removeStrategy=function(a){return delete b[a],this},this.useStrategy=function(a){return d=!0,c=a,this},this.$get=["$injector","$log",function(f,g){var h={},i=function(a,c,d){return angular.forEach(d,function(d){if(angular.isFunction(d))a=d(a,c);else if(angular.isFunction(b[d]))a=b[d](a,c);else{if(!angular.isString(b[d]))throw new Error("pascalprecht.translate.$translateSanitization: Unknown sanitization strategy: '"+d+"'");if(!h[b[d]])try{h[b[d]]=f.get(b[d])}catch(e){throw h[b[d]]=function(){},new Error("pascalprecht.translate.$translateSanitization: Unknown sanitization strategy: '"+d+"'")}a=h[b[d]](a,c)}}),a},j=function(){d||e||(g.warn("pascalprecht.translate.$translateSanitization: No sanitization strategy has been configured. This can have serious security implications. See http://angular-translate.github.io/docs/#/guide/19_security for details."),e=!0)};return f.has("$sanitize")&&(a=f.get("$sanitize")),{useStrategy:function(a){return function(b){a.useStrategy(b)}}(this),sanitize:function(a,b,d){if(c||j(),arguments.length<3&&(d=c),!d)return a;var e=angular.isArray(d)?d:[d];return i(a,b,e)}}}];var f=function(a){var b=angular.element("<div></div>");return b.text(a),b.html()},g=function(b){if(!a)throw new Error("pascalprecht.translate.$translateSanitization: Error cannot find $sanitize service. Either include the ngSanitize module (https://docs.angularjs.org/api/ngSanitize) or use a sanitization strategy which does not depend on $sanitize, such as 'escape'.");return a(b)},h=function(a,b){if(angular.isObject(a)){var c=angular.isArray(a)?[]:{};return angular.forEach(a,function(a,d){c[d]=h(a,b)}),c}return angular.isNumber(a)?a:b(a)}}function c(a,b,c,d){"use strict";var e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t={},u=[],v=a,w=[],x="translate-cloak",y=!1,z=!1,A=".",B=!1,C=0,D=!0,E="default",F={"default":function(a){return(a||"").split("-").join("_")},java:function(a){var b=(a||"").split("-").join("_"),c=b.split("_");return c.length>1?c[0].toLowerCase()+"_"+c[1].toUpperCase():b},bcp47:function(a){var b=(a||"").split("_").join("-"),c=b.split("-");return c.length>1?c[0].toLowerCase()+"-"+c[1].toUpperCase():b}},G="2.8.1",H=function(){if(angular.isFunction(d.getLocale))return d.getLocale();var a,c,e=b.$get().navigator,f=["language","browserLanguage","systemLanguage","userLanguage"];if(angular.isArray(e.languages))for(a=0;a<e.languages.length;a++)if(c=e.languages[a],c&&c.length)return c;for(a=0;a<f.length;a++)if(c=e[f[a]],c&&c.length)return c;return null};H.displayName="angular-translate/service: getFirstBrowserLanguage";var I=function(){var a=H()||"";return F[E]&&(a=F[E](a)),a};I.displayName="angular-translate/service: getLocale";var J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K=function(){return this.toString().replace(/^\s+|\s+$/g,"")},L=function(a){for(var b=[],c=angular.lowercase(a),d=0,e=u.length;e>d;d++)b.push(angular.lowercase(u[d]));if(J(b,c)>-1)return a;if(f){var g;for(var h in f){var i=!1,j=Object.prototype.hasOwnProperty.call(f,h)&&angular.lowercase(h)===angular.lowercase(a);if("*"===h.slice(-1)&&(i=h.slice(0,-1)===a.slice(0,h.length-1)),(j||i)&&(g=f[h],J(b,angular.lowercase(g))>-1))return g}}if(a){var k=a.split("_");if(k.length>1&&J(b,angular.lowercase(k[0]))>-1)return k[0]}return a},M=function(a,b){if(!a&&!b)return t;if(a&&!b){if(angular.isString(a))return t[a]}else angular.isObject(t[a])||(t[a]={}),angular.extend(t[a],N(b));return this};this.translations=M,this.cloakClassName=function(a){return a?(x=a,this):x},this.nestedObjectDelimeter=function(a){return a?(A=a,this):A};var N=function(a,b,c,d){var e,f,g,h;b||(b=[]),c||(c={});for(e in a)Object.prototype.hasOwnProperty.call(a,e)&&(h=a[e],angular.isObject(h)?N(h,b.concat(e),c,e):(f=b.length?""+b.join(A)+A+e:e,b.length&&e===d&&(g=""+b.join(A),c[g]="@:"+f),c[f]=h));return c};N.displayName="flatObject",this.addInterpolation=function(a){return w.push(a),this},this.useMessageFormatInterpolation=function(){return this.useInterpolation("$translateMessageFormatInterpolation")},this.useInterpolation=function(a){return n=a,this},this.useSanitizeValueStrategy=function(a){return c.useStrategy(a),this},this.preferredLanguage=function(a){return a?(O(a),this):e};var O=function(a){return a&&(e=a),e};this.translationNotFoundIndicator=function(a){return this.translationNotFoundIndicatorLeft(a),this.translationNotFoundIndicatorRight(a),this},this.translationNotFoundIndicatorLeft=function(a){return a?(q=a,this):q},this.translationNotFoundIndicatorRight=function(a){return a?(r=a,this):r},this.fallbackLanguage=function(a){return P(a),this};var P=function(a){return a?(angular.isString(a)?(h=!0,g=[a]):angular.isArray(a)&&(h=!1,g=a),angular.isString(e)&&J(g,e)<0&&g.push(e),this):h?g[0]:g};this.use=function(a){if(a){if(!t[a]&&!o)throw new Error("$translateProvider couldn't find translationTable for langKey: '"+a+"'");return i=a,this}return i};var Q=function(a){return a?(v=a,this):l?l+v:v};this.storageKey=Q,this.useUrlLoader=function(a,b){return this.useLoader("$translateUrlLoader",angular.extend({url:a},b))},this.useStaticFilesLoader=function(a){return this.useLoader("$translateStaticFilesLoader",a)},this.useLoader=function(a,b){return o=a,p=b||{},this},this.useLocalStorage=function(){return this.useStorage("$translateLocalStorage")},this.useCookieStorage=function(){return this.useStorage("$translateCookieStorage")},this.useStorage=function(a){return k=a,this},this.storagePrefix=function(a){return a?(l=a,this):a},this.useMissingTranslationHandlerLog=function(){return this.useMissingTranslationHandler("$translateMissingTranslationHandlerLog")},this.useMissingTranslationHandler=function(a){return m=a,this},this.usePostCompiling=function(a){return y=!!a,this},this.forceAsyncReload=function(a){return z=!!a,this},this.uniformLanguageTag=function(a){return a?angular.isString(a)&&(a={standard:a}):a={},E=a.standard,this},this.determinePreferredLanguage=function(a){var b=a&&angular.isFunction(a)?a():I();return e=u.length?L(b):b,this},this.registerAvailableLanguageKeys=function(a,b){return a?(u=a,b&&(f=b),this):u},this.useLoaderCache=function(a){return a===!1?s=void 0:a===!0?s=!0:"undefined"==typeof a?s="$translationCache":a&&(s=a),this},this.directivePriority=function(a){return void 0===a?C:(C=a,this)},this.statefulFilter=function(a){return void 0===a?D:(D=a,this)},this.$get=["$log","$injector","$rootScope","$q",function(a,b,c,d){var f,l,u,E=b.get(n||"$translateDefaultInterpolation"),F=!1,H={},I={},R=function(a,b,c,h){if(angular.isArray(a)){var j=function(a){for(var e={},f=[],g=function(a){var f=d.defer(),g=function(b){e[a]=b,f.resolve([a,b])};return R(a,b,c,h).then(g,g),f.promise},i=0,j=a.length;j>i;i++)f.push(g(a[i]));return d.all(f).then(function(){return e})};return j(a)}var m=d.defer();a&&(a=K.apply(a));var n=function(){var a=e?I[e]:I[i];if(l=0,k&&!a){var b=f.get(v);if(a=I[b],g&&g.length){var c=J(g,b);l=0===c?1:0,J(g,e)<0&&g.push(e)}}return a}();if(n){var o=function(){ca(a,b,c,h).then(m.resolve,m.reject)};o.displayName="promiseResolved",n["finally"](o,m.reject)}else ca(a,b,c,h).then(m.resolve,m.reject);return m.promise},S=function(a){return q&&(a=[q,a].join(" ")),r&&(a=[a,r].join(" ")),a},T=function(a){i=a,k&&f.put(R.storageKey(),i),c.$emit("$translateChangeSuccess",{language:a}),E.setLocale(i);var b=function(a,b){H[b].setLocale(i)};b.displayName="eachInterpolatorLocaleSetter",angular.forEach(H,b),c.$emit("$translateChangeEnd",{language:a})},U=function(a){if(!a)throw"No language key specified for loading.";var e=d.defer();c.$emit("$translateLoadingStart",{language:a}),F=!0;var f=s;"string"==typeof f&&(f=b.get(f));var g=angular.extend({},p,{key:a,$http:angular.extend({},{cache:f},p.$http)}),h=function(b){var d={};c.$emit("$translateLoadingSuccess",{language:a}),angular.isArray(b)?angular.forEach(b,function(a){angular.extend(d,N(a))}):angular.extend(d,N(b)),F=!1,e.resolve({key:a,table:d}),c.$emit("$translateLoadingEnd",{language:a})};h.displayName="onLoaderSuccess";var i=function(a){c.$emit("$translateLoadingError",{language:a}),e.reject(a),c.$emit("$translateLoadingEnd",{language:a})};return i.displayName="onLoaderError",b.get(o)(g).then(h,i),e.promise};if(k&&(f=b.get(k),!f.get||!f.put))throw new Error("Couldn't use storage '"+k+"', missing get() or put() method!");if(w.length){var V=function(a){var c=b.get(a);c.setLocale(e||i),H[c.getInterpolationIdentifier()]=c};V.displayName="interpolationFactoryAdder",angular.forEach(w,V)}var W=function(a){var b=d.defer();if(Object.prototype.hasOwnProperty.call(t,a))b.resolve(t[a]);else if(I[a]){var c=function(a){M(a.key,a.table),b.resolve(a.table)};c.displayName="translationTableResolver",I[a].then(c,b.reject)}else b.reject();return b.promise},X=function(a,b,c,e){var f=d.defer(),g=function(d){if(Object.prototype.hasOwnProperty.call(d,b)){e.setLocale(a);var g=d[b];"@:"===g.substr(0,2)?X(a,g.substr(2),c,e).then(f.resolve,f.reject):f.resolve(e.interpolate(d[b],c)),e.setLocale(i)}else f.reject()};return g.displayName="fallbackTranslationResolver",W(a).then(g,f.reject),f.promise},Y=function(a,b,c,d){var e,f=t[a];if(f&&Object.prototype.hasOwnProperty.call(f,b)){if(d.setLocale(a),e=d.interpolate(f[b],c),"@:"===e.substr(0,2))return Y(a,e.substr(2),c,d);d.setLocale(i)}return e},Z=function(a,c){if(m){var d=b.get(m)(a,i,c);return void 0!==d?d:a}return a},$=function(a,b,c,e,f){var h=d.defer();if(a<g.length){var i=g[a];X(i,b,c,e).then(h.resolve,function(){$(a+1,b,c,e,f).then(h.resolve)})}else f?h.resolve(f):h.resolve(Z(b,c));return h.promise},_=function(a,b,c,d){var e;if(a<g.length){var f=g[a];e=Y(f,b,c,d),e||(e=_(a+1,b,c,d))}return e},aa=function(a,b,c,d){return $(u>0?u:l,a,b,c,d)},ba=function(a,b,c){return _(u>0?u:l,a,b,c)},ca=function(a,b,c,e){var f=d.defer(),h=i?t[i]:t,j=c?H[c]:E;if(h&&Object.prototype.hasOwnProperty.call(h,a)){var k=h[a];"@:"===k.substr(0,2)?R(k.substr(2),b,c,e).then(f.resolve,f.reject):f.resolve(j.interpolate(k,b))}else{var l;m&&!F&&(l=Z(a,b)),i&&g&&g.length?aa(a,b,j,e).then(function(a){f.resolve(a)},function(a){f.reject(S(a))}):m&&!F&&l?e?f.resolve(e):f.resolve(l):e?f.resolve(e):f.reject(S(a))}return f.promise},da=function(a,b,c){var d,e=i?t[i]:t,f=E;if(H&&Object.prototype.hasOwnProperty.call(H,c)&&(f=H[c]),e&&Object.prototype.hasOwnProperty.call(e,a)){var h=e[a];d="@:"===h.substr(0,2)?da(h.substr(2),b,c):f.interpolate(h,b)}else{var j;m&&!F&&(j=Z(a,b)),i&&g&&g.length?(l=0,d=ba(a,b,f)):d=m&&!F&&j?j:S(a)}return d},ea=function(a){j===a&&(j=void 0),I[a]=void 0};R.preferredLanguage=function(a){return a&&O(a),e},R.cloakClassName=function(){return x},R.nestedObjectDelimeter=function(){return A},R.fallbackLanguage=function(a){if(void 0!==a&&null!==a){if(P(a),o&&g&&g.length)for(var b=0,c=g.length;c>b;b++)I[g[b]]||(I[g[b]]=U(g[b]));R.use(R.use())}return h?g[0]:g},R.useFallbackLanguage=function(a){if(void 0!==a&&null!==a)if(a){var b=J(g,a);b>-1&&(u=b)}else u=0},R.proposedLanguage=function(){return j},R.storage=function(){return f},R.use=function(a){if(!a)return i;var b=d.defer();c.$emit("$translateChangeStart",{language:a});var e=L(a);return e&&(a=e),!z&&t[a]||!o||I[a]?j===a&&I[a]?I[a].then(function(a){return b.resolve(a.key),a},function(a){return b.reject(a),d.reject(a)}):(b.resolve(a),T(a)):(j=a,I[a]=U(a).then(function(c){return M(c.key,c.table),b.resolve(c.key),j===a&&T(c.key),c},function(a){return c.$emit("$translateChangeError",{language:a}),b.reject(a),c.$emit("$translateChangeEnd",{language:a}),d.reject(a)}),I[a]["finally"](function(){ea(a)})),b.promise},R.storageKey=function(){return Q()},R.isPostCompilingEnabled=function(){return y},R.isForceAsyncReloadEnabled=function(){return z},R.refresh=function(a){function b(){f.resolve(),c.$emit("$translateRefreshEnd",{language:a})}function e(){f.reject(),c.$emit("$translateRefreshEnd",{language:a})}if(!o)throw new Error("Couldn't refresh translation table, no loader registered!");var f=d.defer();if(c.$emit("$translateRefreshStart",{language:a}),a)if(t[a]){var h=function(c){M(c.key,c.table),a===i&&T(i),b()};h.displayName="refreshPostProcessor",U(a).then(h,e)}else e();else{var j=[],k={};if(g&&g.length)for(var l=0,m=g.length;m>l;l++)j.push(U(g[l])),k[g[l]]=!0;i&&!k[i]&&j.push(U(i));var n=function(a){t={},angular.forEach(a,function(a){M(a.key,a.table)}),i&&T(i),b()};n.displayName="refreshPostProcessor",d.all(j).then(n,e)}return f.promise},R.instant=function(a,b,c){if(null===a||angular.isUndefined(a))return a;if(angular.isArray(a)){for(var d={},f=0,h=a.length;h>f;f++)d[a[f]]=R.instant(a[f],b,c);return d}if(angular.isString(a)&&a.length<1)return a;a&&(a=K.apply(a));var j,k=[];e&&k.push(e),i&&k.push(i),g&&g.length&&(k=k.concat(g));for(var l=0,n=k.length;n>l;l++){var o=k[l];if(t[o]&&("undefined"!=typeof t[o][a]?j=da(a,b,c):(q||r)&&(j=S(a))),"undefined"!=typeof j)break}return j||""===j||(j=E.interpolate(a,b),m&&!F&&(j=Z(a,b))),j},R.versionInfo=function(){return G},R.loaderCache=function(){return s},R.directivePriority=function(){return C},R.statefulFilter=function(){return D},R.isReady=function(){return B};var fa=d.defer();fa.promise.then(function(){B=!0}),R.onReady=function(a){var b=d.defer();return angular.isFunction(a)&&b.promise.then(a),B?b.resolve():fa.promise.then(b.resolve),b.promise};var ga=c.$on("$translateReady",function(){fa.resolve(),ga(),ga=null}),ha=c.$on("$translateChangeEnd",function(){fa.resolve(),ha(),ha=null});if(o){if(angular.equals(t,{})&&R.use()&&R.use(R.use()),g&&g.length)for(var ia=function(a){return M(a.key,a.table),c.$emit("$translateChangeEnd",{language:a.key}),a},ja=0,ka=g.length;ka>ja;ja++){var la=g[ja];(z||!t[la])&&(I[la]=U(la).then(ia))}}else c.$emit("$translateReady",{language:R.use()});return R}]}function d(a,b){"use strict";var c,d={},e="default";return d.setLocale=function(a){c=a},d.getInterpolationIdentifier=function(){return e},d.useSanitizeValueStrategy=function(a){return b.useStrategy(a),this},d.interpolate=function(c,d){d=d||{},d=b.sanitize(d,"params");var e=a(c)(d);return e=b.sanitize(e,"text")},d}function e(a,b,c,d,e,g){"use strict";var h=function(){return this.toString().replace(/^\s+|\s+$/g,"")};return{restrict:"AE",scope:!0,priority:a.directivePriority(),compile:function(b,i){var j=i.translateValues?i.translateValues:void 0,k=i.translateInterpolation?i.translateInterpolation:void 0,l=b[0].outerHTML.match(/translate-value-+/i),m="^(.*)("+c.startSymbol()+".*"+c.endSymbol()+")(.*)",n="^(.*)"+c.startSymbol()+"(.*)"+c.endSymbol()+"(.*)";return function(b,o,p){b.interpolateParams={},b.preText="",b.postText="",b.translateNamespace=f(b);var q={},r=function(a,c,d){if(c.translateValues&&angular.extend(a,e(c.translateValues)(b.$parent)),l)for(var f in d)if(Object.prototype.hasOwnProperty.call(c,f)&&"translateValue"===f.substr(0,14)&&"translateValues"!==f){var g=angular.lowercase(f.substr(14,1))+f.substr(15);a[g]=d[f]}},s=function(a){if(angular.isFunction(s._unwatchOld)&&(s._unwatchOld(),s._unwatchOld=void 0),angular.equals(a,"")||!angular.isDefined(a)){var d=h.apply(o.text()),e=d.match(m);if(angular.isArray(e)){b.preText=e[1],b.postText=e[3],q.translate=c(e[2])(b.$parent);var f=d.match(n);angular.isArray(f)&&f[2]&&f[2].length&&(s._unwatchOld=b.$watch(f[2],function(a){q.translate=a,y()}))}else q.translate=d}else q.translate=a;y()},t=function(a){p.$observe(a,function(b){q[a]=b,y()})};r(b.interpolateParams,p,i);var u=!0;p.$observe("translate",function(a){"undefined"==typeof a?s(""):""===a&&u||(q.translate=a,y()),u=!1});for(var v in p)p.hasOwnProperty(v)&&"translateAttr"===v.substr(0,13)&&t(v);if(p.$observe("translateDefault",function(a){b.defaultText=a}),j&&p.$observe("translateValues",function(a){a&&b.$parent.$watch(function(){angular.extend(b.interpolateParams,e(a)(b.$parent))})}),l){var w=function(a){p.$observe(a,function(c){var d=angular.lowercase(a.substr(14,1))+a.substr(15);b.interpolateParams[d]=c})};for(var x in p)Object.prototype.hasOwnProperty.call(p,x)&&"translateValue"===x.substr(0,14)&&"translateValues"!==x&&w(x)}var y=function(){for(var a in q)q.hasOwnProperty(a)&&void 0!==q[a]&&z(a,q[a],b,b.interpolateParams,b.defaultText,b.translateNamespace)},z=function(b,c,d,e,f,g){c?(g&&"."===c.charAt(0)&&(c=g+c),a(c,e,k,f).then(function(a){A(a,d,!0,b)},function(a){A(a,d,!1,b)})):A(c,d,!1,b)},A=function(b,c,e,f){if("translate"===f){e||"undefined"==typeof c.defaultText||(b=c.defaultText),o.empty().append(c.preText+b+c.postText);var g=a.isPostCompilingEnabled(),h="undefined"!=typeof i.translateCompile,j=h&&"false"!==i.translateCompile;(g&&!h||j)&&d(o.contents())(c)}else{e||"undefined"==typeof c.defaultText||(b=c.defaultText);var k=p.$attr[f];"data-"===k.substr(0,5)&&(k=k.substr(5)),k=k.substr(15),o.attr(k,b)}};(j||l||p.translateDefault)&&b.$watch("interpolateParams",y,!0);var B=g.$on("$translateChangeSuccess",y);o.text().length?s(p.translate?p.translate:""):p.translate&&s(p.translate),y(),b.$on("$destroy",B)}}}}function f(a){"use strict";return a.translateNamespace?a.translateNamespace:a.$parent?f(a.$parent):void 0}function g(a){"use strict";return{compile:function(b){var c=function(){b.addClass(a.cloakClassName())},d=function(){b.removeClass(a.cloakClassName())};return a.onReady(function(){d()}),c(),function(b,e,f){f.translateCloak&&f.translateCloak.length&&f.$observe("translateCloak",function(b){a(b).then(d,c)})}}}}function h(){"use strict";return{restrict:"A",scope:!0,compile:function(){return{pre:function(a,b,c){a.translateNamespace=f(a),a.translateNamespace&&"."===c.translateNamespace.charAt(0)?a.translateNamespace+=c.translateNamespace:a.translateNamespace=c.translateNamespace}}}}}function f(a){"use strict";return a.translateNamespace?a.translateNamespace:a.$parent?f(a.$parent):void 0}function i(a,b){"use strict";var c=function(c,d,e){return angular.isObject(d)||(d=a(d)(this)),b.instant(c,d,e)};return b.statefulFilter()&&(c.$stateful=!0),c}function j(a){"use strict";return a("translations")}return angular.module("pascalprecht.translate",["ng"]).run(a),a.$inject=["$translate"],a.displayName="runTranslate",angular.module("pascalprecht.translate").provider("$translateSanitization",b),angular.module("pascalprecht.translate").constant("pascalprechtTranslateOverrider",{}).provider("$translate",c),c.$inject=["$STORAGE_KEY","$windowProvider","$translateSanitizationProvider","pascalprechtTranslateOverrider"],c.displayName="displayName",angular.module("pascalprecht.translate").factory("$translateDefaultInterpolation",d),d.$inject=["$interpolate","$translateSanitization"],d.displayName="$translateDefaultInterpolation",angular.module("pascalprecht.translate").constant("$STORAGE_KEY","NG_TRANSLATE_LANG_KEY"),angular.module("pascalprecht.translate").directive("translate",e),e.$inject=["$translate","$q","$interpolate","$compile","$parse","$rootScope"],e.displayName="translateDirective",angular.module("pascalprecht.translate").directive("translateCloak",g),g.$inject=["$translate"],g.displayName="translateCloakDirective",angular.module("pascalprecht.translate").directive("translateNamespace",h),h.displayName="translateNamespaceDirective",angular.module("pascalprecht.translate").filter("translate",i),i.$inject=["$parse","$translate"],i.displayName="translateFilterFactory",angular.module("pascalprecht.translate").factory("$translationCache",j),j.$inject=["$cacheFactory"],j.displayName="$translationCache","pascalprecht.translate"});
/*!
 * angular-translate - v2.8.1 - 2015-10-01
 * 
 * Copyright (c) 2015 The angular-translate team, Pascal Precht; Licensed MIT
 */
!function(a,b){"function"==typeof define&&define.amd?define([],function(){return b()}):"object"==typeof exports?module.exports=b():b()}(this,function(){function a(a,b){"use strict";return function(c){if(!(c&&(angular.isArray(c.files)||angular.isString(c.prefix)&&angular.isString(c.suffix))))throw new Error("Couldn't load static files, no files and prefix or suffix specified!");c.files||(c.files=[{prefix:c.prefix,suffix:c.suffix}]);for(var d=function(d){if(!d||!angular.isString(d.prefix)||!angular.isString(d.suffix))throw new Error("Couldn't load static file, no prefix or suffix specified!");return b(angular.extend({url:[d.prefix,c.key,d.suffix].join(""),method:"GET",params:""},c.$http)).then(function(a){return a.data},function(){return a.reject(c.key)})},e=a.defer(),f=[],g=c.files.length,h=0;g>h;h++)f.push(d({prefix:c.files[h].prefix,key:c.key,suffix:c.files[h].suffix}));return a.all(f).then(function(a){for(var b=a.length,c={},d=0;b>d;d++)for(var f in a[d])c[f]=a[d][f];e.resolve(c)},function(a){e.reject(a)}),e.promise}}return angular.module("pascalprecht.translate").factory("$translateStaticFilesLoader",a),a.$inject=["$q","$http"],a.displayName="$translateStaticFilesLoader","pascalprecht.translate"});
angular.module("uuid",[]).factory("rfc4122",function(){function r(r){return Math.random()*r}return{v4:function(){var n,a="";for(n=0;36>n;n++)a+=14===n?"4":19===n?"89ab".charAt(r(4)):8===n||13===n||18===n||23===n?"-":"0123456789abcdef".charAt(r(16));return a}}});
(function () {
  'use strict';

  angular.module('counterMax', [

    'ionic',
    'counterMax.routes',
    'counterMax.services',
    'counterMax.controllers',
    'counterMax.video',
    'counterMax.dashboard',
    'counterMax.account',
    'counterMax.settings',
    'counterMax.visitas',
    'ngCordova',
    'angular-oauth2',
    'templates',
    'pascalprecht.translate'  // inject the angular-translate module


  ]);
  angular.module('counterMax.services', []);
  angular.module('counterMax.routes', []);
  angular.module('counterMax.controllers', []);
  angular.module('counterMax').config(config);
  config.$inject = ['$ionicConfigProvider', '$translateProvider', 'OAuthProvider'];
  angular.module('counterMax').run(run);
  run.$inject = [
    '$ionicPlatform',
    '$cordovaPushV5',
    '$rootScope',
    '$cordovaToast',
    '$ionicHistory',
    '$state',
    '$translate',
    '$filter',
    '$cordovaGoogleAds',
    'OAuth',
    'databaseService'];

  /**
   * @name config
   * @desc Configure all factories
   * @param $ionicConfigProvider
   * @param $translateProvider
   * @param OAuthProvider
   */
  function config($ionicConfigProvider, $translateProvider, OAuthProvider) {


    OAuthProvider.configure({
      baseUrl: 'http://www.google.es',
      clientId: '1232',
      clientSecret: null,
      grantPath: '/oauth2/token',
      revokePath: '/oauth2/revoke'
    });

    $ionicConfigProvider.navBar.alignTitle('center');
    $translateProvider
      .useStaticFilesLoader({
        prefix: 'locales/',
        suffix: '.json'
      })
      .registerAvailableLanguageKeys(['es', 'en'], {
        'en': 'en', 'en_GB': 'en', 'en_US': 'en',
        'es': 'es', 'es_ES': 'es', 'es_AR': 'es'
      })
      .preferredLanguage('es')
      .fallbackLanguage('en')
      .determinePreferredLanguage()
      .useSanitizeValueStrategy('escapeParameters');
  }


  /**
   * @name run
   */
  function run($ionicPlatform,
               $cordovaPushV5,
               $rootScope,
               $cordovaToast,
               $ionicHistory,
               $state,
               $translate,
               $filter,
               $cordovaGoogleAds,
               OAuth,
               databaseService) {

    var countTimerForCloseApp = false;


    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      if(window.cordova) {
        registerBackButton($ionicPlatform, $cordovaToast, $state, $ionicHistory, $filter, countTimerForCloseApp);
        registerPush($cordovaPushV5, $rootScope);
        databaseService.createTableVisit();
        prepareTranslation($translate);

        oauthErrors($rootScope, OAuth);

        prepareAds($cordovaGoogleAds);

      }
      if (window.localStorage.loginCorrect === "true") {
        $state.go('tab.video');

      } else {

        $state.go('auth');
      }


    });

  }

  /**
   * @name prepareTranlation
   * @desc carga el archivo de traduccion
   * @param $translate
   */
  function prepareTranslation($translate) {
    if (typeof navigator.globalization !== "undefined") {
      navigator.globalization.getPreferredLanguage(function (language) {
        var language_list = {"French": "es", "English": "en", "Spanish": "es", "Chinese": "zh", "Japanese": "ja"};
        var language_value = (language.value).split(/[\s,-]+/)[0];

        if (language_list.hasOwnProperty(language_value)) {
          var language_locale = language_list[language_value];
          $translate.use(language_locale).then(function (data) {
            console.log("SUCCESS -> " + data);
          }, function (error) {
            console.log("ERROR -> " + error);
          });
        }
      }, null);
    }

  }

  /**
   * @name prepareAds
   * @desc prepara la publicidad
   * @param $cordovaGoogleAds
   */
  function prepareAds($cordovaGoogleAds) {
    var admobid = {};
    //Banner configuration
    admobid = {
      banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
      interstitial: 'ca-app-pub-2135463162242237/3946497300'
    };
    if (ionic.Platform.isIOS()) {
      cordova.plugins.iosrtc.registerGlobals();
      admobid = {
        banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2135463162242237/3946497300'
      };
    }
    if (ionic.Platform.isAndroid()) {
      admobid = {
        banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2135463162242237/3946497300'
      };

    }
    if (ionic.Platform.isWindowsPhone()) {
      admobid = {
        banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2135463162242237/3946497300'
      };
    }

    if (ionic.Platform.isWebView()) {
      admobid = {
        banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2135463162242237/3946497300'
      };
    }
    var banner_options = {
      adSize: 'SMART_BANNER',
      width: 360, // valid when set adSize 'CUSTOM'
      height: 90, // valid when set adSize 'CUSTOM'
      position: 8,
      x: 0,       // valid when set position to POS_XY
      y: 0,       // valid when set position to POS_XY
      isTesting: false,
      autoShow: true
    };
    $cordovaGoogleAds.setOptions(banner_options).then(creobannerOK, creobannerKO);
    function creobannerOK() {

      $cordovaGoogleAds.createBanner(admobid.banner).then(function (ok) {
        $cordovaGoogleAds.showBanner(8);
      }, function (err) {
        console.log('err');
      });
      $cordovaGoogleAds.prepareInterstitial({adId: admobid.interstitial, autoShow: false,isTesting: false});
    }

    function creobannerKO() {
      console.log("No puedo hacer nada");
    }


  }


  function oauthErrors($rootScope, OAuth) {
    $rootScope.$on('oauth:error', function (event, rejection) {
      // Ignore `invalid_grant` error - should be catched on `LoginController`.
      if ('invalid_grant' === rejection.data.error) {
        return;
      }

      // Refresh token when a `invalid_token` error occurs.
      if ('invalid_token' === rejection.data.error) {
        return OAuth.getRefreshToken();
      }

    });

  }

  function registerBackButton($ionicPlatform, $cordovaToast, $state, $ionicHistory, $filter, countTimerForCloseApp) {
    var $translate = $filter('translate');
    $ionicPlatform.registerBackButtonAction(function (e) {
      e.preventDefault();
      function showConfirm() {
        if (countTimerForCloseApp) {
          ionic.Platform.exitApp();
        } else {
          $cordovaToast.showLongBottom($translate('CONFIRM_EXIT')).then(function (success) {
            countTimerForCloseApp = true;
            $timeout(function () {
              countTimerForCloseApp = false;
            }, 2000, true, countTimerForCloseApp);

          }, function (error) {
            // error
          });
        }

      }

      // Is there a page to go back to?
      if (($ionicHistory.backView()) && !($state.is('tab.tutorial') || $state.is('tab.video'))) {
        // Go back in history
        $ionicHistory.backView().go();
      } else {
        // This is the last page: Show confirmation popup
        showConfirm();
      }

      return false;
    }, 101);
  }

  /**
   * Controla el sistema de push
   * @param $cordovaPushV5
   * @param $rootScope
   */
  function registerPush($cordovaPushV5, $rootScope) {

      var pushOptions = {
        android: {
          senderID: "822255661242",
          icon: "icon",
          iconColor: "#008e94"
        },
        ios: {
          alert: "true",
          badge: "true",
          sound: "true"
        },
        windows: {}
      };



      $cordovaPushV5.initialize(pushOptions);
      $cordovaPushV5.register().then(function (token) {
        console.log('$cordovaPushV5:REGISTERED', token);
      }, function (err) {
        console.error('$cordovaPushV5:REGISTER_ERROR', err);
      });


      $rootScope.$on('$cordovaPushV5:notificationReceived', function (event, notification) {
        switch (notification.event) {
          case 'registered':
            if (notification.regid.length > 0) {
              alert('registration ID = ' + notification.regid);
            }
            break;

          case 'message':
            // this is the actual push notification. its format depends on the data model from the push server
            alert('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
            break;

          case 'error':
            alert('GCM error = ' + notification.msg);
            break;

          default:
            alert('An unknown GCM event has occurred');
            break;
        }
      });




  }
})();

angular.module("templates", []).run(["$templateCache", function($templateCache) {$templateCache.put("login.html","<ion-view title=\"COUNTERMAX\">\n  <ion-content class=\"padding\">\n    <img src=\"img/ojo.png\" style=\"width:100%\">\n    <div class=\"list\">\n      <button class=\"button button-full icon-left ion-social-facebook button-facebook\" ng-click=\"facebookLogin()\">\n        <small translate=\"LOGIN_FACEBOOK\"></small>\n      </button>\n    </div>\n\n    <div class=\"list card\">\n      <div class=\"item item-divider\"><small translate=\"LOGIN_REGISTER\"></small></div>\n      <div class=\"item item-body\">\n        <div>\n          <label class=\"item item-input\">\n            <span class=\"input-label\">Email</span>\n            <input type=\"text\"  ng-model=\"email\">\n          </label>\n          <label class=\"item item-input item-icon-right\">\n            <span class=\"input-label\" translate=\"PASSWORD\"></span>\n            <i class=\"icon ion-eye\" on-touch=\"showPassword = !showPassword\" ng-show=\"showPassword\"></i>\n            <i class=\"icon ion-eye-disabled placeholder-icon\" on-touch=\"showPassword = !showPassword\" ng-hide=\"showPassword\"></i>\n            <input type=\"password\" id=\"password\" placeholder=\"{{PASSWORD |translate}}\" ng-model=\"password\" ng-hide=\"showPassword\">\n            <input type=\"text\" id=\"passwordRaw\" placeholder=\"{{PASSWORD |translate}}\" ng-model=\"password\" ng-show=\"showPassword\">\n          </label>\n          <label class=\"item item-input\">\n            <button class=\"button button-full button-stable\" ng-click=\"normalLogin()\">\n             {{\'ENTRAR_REGISTRO\' | translate}}\n            </button>\n          </label>\n        </div>\n      </div>\n    </div>\n\n\n  </ion-content>\n\n</ion-view>\n");
$templateCache.put("tab-settings.html","<ion-view>\n  <ion-nav-title>\n    <small translate=\"MENU_SETTINGS\"></small>\n  </ion-nav-title>\n  <ion-content class=\"padding\">\n    <div class=\"item item-divider\">\n      Configuración básica\n    </div>\n    <label class=\"item item-input\">\n      <span class=\"input-label\">Analytics ID</span>\n      <input type=\"text\" ng-model=\"settings.analyticsID\" placeholder=\"UA-00000-00\">\n    </label>\n\n    <label class=\"item item-input item-select\">\n      <div class=\"input-label\">\n        Elija Sonido\n      </div>\n      <select ng-model=\"settings.sound\" ng-change=\"updateSound()\">\n        <option value=\"sounds/Ariel.mp3\">Ariel</option>\n        <option value=\"sounds/Carme.mp3\">Carme</option>\n        <option value=\"sounds/Ceres.mp3\">Ceres</option>\n        <option value=\"sounds/Elara.mp3\">Elara</option>\n        <option value=\"sounds/Europa.mp3\">Europa</option>\n        <option value=\"sounds/Iapetus.mp3\">Iapetus</option>\n        <option value=\"sounds/Io.mp3\">Io</option>\n        <option value=\"sounds/Rhea.mp3\">Rhea</option>\n        <option value=\"sounds/Salacia.mp3\">Salacia</option>\n        <option value=\"sounds/Tethys.mp3\">Tethys</option>\n        <option value=\"sounds/Titan.mp3\">Titan</option>\n\n\n      </select>\n    </label>\n\n    <label class=\"item item-input item-select\">\n      <div class=\"input-label\">\n        Elija disposición\n      </div>\n      <select ng-model=\"settings.disposition\">\n        <option value=0>Derecha</option>\n        <option value=1>Izquierda</option>\n\n\n      </select>\n    </label>\n    <button class=\"button button-full button-stable\" ng-click=\"saveOptions()\">\n      Grabar\n    </button>\n  </ion-content>\n</ion-view>\n");
$templateCache.put("tab-tutorial.html","<ion-view view-title=\"Dashboard\">\n  <ion-nav-title>\n    <small translate=\"MENU_TUTORIAL\"></small>\n  </ion-nav-title>\n  <ion-content>\n    <ion-slide-box on-slide-changed=\"slideHasChanged($index)\">\n      <ion-slide class=\"padding\">\n        <h3><i class=\"ion-ios-gear-outline\" style=\"margin-right:5px\"></i>{{\"MENU_SETTINGS\"|translate}}</h3>\n        <div class=\"image_tutorial\">\n          <img src=\"img/tutorial_2.png\">\n        </div>\n        <span data-ng-bind-html=\"\'SLIDE_1_TEXT\'|translate\"></span>\n\n      </ion-slide>\n      <ion-slide class=\"padding\">\n        <h3><i class=\"ion-ios-eye-outline\" style=\"margin-right:4px\"></i>{{\"MENU_VIDEO\"|translate}}</h3>\n        <div class=\"image_tutorial\">\n        <img src=\"img/tutorial_1.png\">\n        </div>\n        <span data-ng-bind-html=\"\'SLIDE_2_TEXT\'|translate\"></span>\n\n      </ion-slide>\n      <ion-slide class=\"padding\">\n        <h3><i class=\"ion-ios-pie-outline\" style=\"margin-right:4px\"></i>{{\"MENU_VISITAS\"|translate}}</h3>\n        <div class=\"image_tutorial\">\n          <img src=\"img/tutorial_3.png\">\n        </div>\n        <span data-ng-bind-html=\"\'SLIDE_3_TEXT\'|translate\"></span>\n         </ion-slide>\n    </ion-slide-box>\n\n  </ion-content>\n\n</ion-view>\n");
$templateCache.put("tab-video.html","<ion-view>\n  <ion-nav-title>\n    <small translate=\"MENU_VIDEO\"></small>\n  </ion-nav-title>\n  <ion-content>\n    <img src=\"img/ojo.png\" style=\"width:100%\" ng-hide=\"abierto\">\n    <div class=\"center\" ng-show=\"abierto\">\n      <video id=\"monitor\" ng-src=\"{{videostream}}\" autoplay muted></video>\n\n      <div id=\"canvasLayers\">\n        <canvas id=\"videoCanvas\" width=\"320\" height=\"240\"></canvas>\n        <canvas id=\"layer2\" width=\"320\" height=\"240\"></canvas>\n      </div>\n\n\n      <canvas id=\"blendCanvas\"></canvas>\n    </div>\n    <button class=\"button button-full button-stable\" ng-click=\"captureVideo()\">\n      <small translate=\"VIDEO_CERRAR_TIENDA\" ng-show=\"abierto\"></small>\n      <small translate=\"VIDEO_ABRIR_TIENDA\" ng-hide=\"abierto\"></small>\n    </button>\n  </ion-content>\n</ion-view>\n");
$templateCache.put("tab-visitas.html","<ion-view>\n  <ion-nav-title>\n    <small translate=\"MENU_VISITAS\"></small>\n  </ion-nav-title>\n  <ion-content class=\"padding\">\n    <div class=\"list card\">\n      <div class=\"item item-divider\"> <small translate=\"VISITAS_DIARIAS\"></small></div>\n      <div class=\"item item-body\">\n        <div>\n          <strong>{{visitasHoy}}</strong>\n        </div>\n      </div>\n    </div>\n    <div class=\"list card\">\n      <div class=\"item item-divider\"><small translate=\"VISITAS_MES\"></small></div>\n      <div class=\"item item-body\">\n        <div>\n          <strong>{{visitasMes}}</strong>\n        </div>\n      </div>\n    </div>\n    <div class=\"list card\">\n      <div class=\"item item-divider\"><small translate=\"VISITAS_TOTALES\"></small></div>\n      <div class=\"item item-body\">\n        <div>\n          <strong>{{visitasTotal}}</strong>\n        </div>\n      </div>\n    </div>\n    <button class=\"button button-full  button-assertive\" ng-click=\"salirApp()\"><small translate=\"VISITAS_SALIR\"></small></button>\n  </ion-content>\n\n</ion-view>\n");
$templateCache.put("tabs.html","<!--\nCreate tabs with an icon and label, using the tabs-positive style.\nEach tab\'s child <ion-nav-view> directive will have its own\nnavigation history that also transitions its views in and out.\n-->\n<ion-tabs class=\"tabs-icon-top tabs-color-active-positive\">\n\n  <!-- Dashboard Tab -->\n  <ion-tab title=\"{{ \'MENU_TUTORIAL\' | translate }}\" icon-off=\"ion-ios-list-outline\" icon-on=\"ion-ios-list\" href=\"#/tab/tutorial\">\n    <ion-nav-view name=\"tab-tutorial\"></ion-nav-view>\n  </ion-tab>\n\n\n  <!-- Account Tab -->\n  <ion-tab title=\"{{ \'MENU_SETTINGS\' | translate }}\" icon-off=\"ion-ios-gear-outline\" icon-on=\"ion-ios-gear\" href=\"#/tab/settings\">\n    <ion-nav-view name=\"tab-settings\"></ion-nav-view>\n  </ion-tab>\n\n  <ion-tab title=\"{{ \'MENU_VIDEO\' | translate }}\" icon-off=\"ion-ios-eye-outline\" icon-on=\"ion-ios-eye\" href=\"#/tab/video\">\n    <ion-nav-view name=\"tab-video\"></ion-nav-view>\n  </ion-tab>\n\n\n\n  <!-- Chats Tab -->\n  <ion-tab title=\"{{ \'MENU_VISITAS\' | translate }}\" icon-off=\"ion-ios-pie-outline\" icon-on=\"ion-ios-pie\" href=\"#/tab/visitas\">\n    <ion-nav-view name=\"tab-visitas\"></ion-nav-view>\n  </ion-tab>\n\n\n\n</ion-tabs>\n");}]);
(function () {
  'use strict';

  angular
    .module('counterMax.routes')
    .config(config);

  config.$inject = ['$stateProvider','$urlRouterProvider'];

  /**
   * @name config
   * @desc Enable HTML5 routing
   */
  function config($stateProvider, $urlRouterProvider) {



    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
      .state('auth', {
        url: '/auth',
        cache:false,
        templateUrl: 'login.html',
        controller:'DefaultController'
      })

      .state('intro', {
        url: '/',
        cache:false,
        template:'<div></div>',
        controller:'AuthCtrl'
      })



      // setup an abstract state for the tabs directive
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'tabs.html'

      })

      // Each tab has its own nav history stack:

      .state('tab.tutorial', {
        url: '/tutorial',
        views: {
          'tab-tutorial': {
            templateUrl: 'tab-tutorial.html',
            controller: 'DashCtrl'
          }
        }
      })
      .state('tab.visitas', {
        url: '/visitas',
        cache:false,
        views: {
          'tab-visitas': {
            templateUrl: 'tab-visitas.html',
            controller: 'VisitasCtrl'
          }
        }
      })

      .state('tab.video', {
        url: '/video',
        views: {
          'tab-video': {
            templateUrl: 'tab-video.html',
            controller: 'VideoCtrl'
          }
        }
      })
      .state('tab.settings', {
        url: '/settings',
        views: {
          'tab-settings': {
            templateUrl: 'tab-settings.html',
            controller: 'SettingsCtrl'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');


  }
})();

/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.controllers')
    .controller('DefaultController', DefaultController);

  DefaultController.$inject = ['$scope','$state','$cordovaFacebook'];

  /**
   * @namespace AccountCtrl
   */
  function DefaultController($scope,$state,$cordovaFacebook) {

    $scope.facebookLogin=facebookLogin;
    $scope.normalLogin=normalLogin;
    /*
    activate();
    $scope.facebookLogin=facebookLogin
*/
    window.localStorage.loginCorrect="false";





    function normalLogin() {
      window.localStorage.loginCorrect="true";
      if(window.localStorage.didTutorial === "true") {
        $state.go('tab.video');
      } else {

        $state.go('tab.tutorial');
      }


    }

    function facebookLogin() {
      $cordovaFacebook.login(["public_profile", "email", "user_friends"])
        .then(function(success) {
          window.localStorage.loginCorrect="true";
          if(window.localStorage.didTutorial === "true") {
            $state.go('tab.video');
          } else {

            $state.go('tab.tutorial');
          }



        }, function (error) {
          console.log(error);
        });

    }




  }
})();

/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.controllers')
    .controller('AuthCtrl', AuthCtrl);

  AuthCtrl.$inject = ['$ionicPlatform','$scope','$state'];

  /**
   * @namespace AccountCtrl
   */
  function AuthCtrl($ionicPlatform,$scope,$state) {

    $ionicPlatform.ready(function() {

    });

  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('databaseService', databaseService);

  databaseService.$inject = ['$cordovaSQLite', '$window'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function databaseService($cordovaSQLite, $window) {
    var db = null;
    return {
      openDb:openDb,
      createTableVisit: createTableVisit,
      addVisit: addVisit,
      todayVisit: todayVisit,
      monthVisit: monthVisit,
      globalVisit: globalVisit,
      closeDb: closeDb

    };

    function openDb() {
      if(window.cordova) {
        // App syntax
        db = $cordovaSQLite.openDB({name: "my.db", androidDatabaseImplementation: 2, androidLockWorkaround: 1});
      } else {
        // Ionic serve syntax
        db = window.openDatabase("my.db", "1.0", "My app", -1);
      }
    }

    function closeDb() {
      var db = accessDB();
      db.close();
    }

    /**
     *
     * @returns promise
     */
    function createTableVisit() {

      var query = "CREATE TABLE if not exists stats " +
        "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
        "date DATETIME DEFAULT (CURRENT_TIMESTAMP))";
      return executeSql(query);
    }

    function addVisit() {
      var query = "insert into stats DEFAULT VALUES";
      return executeSql(query);

    }

    function todayVisit() {
      var queryToday = "select count() as num from stats  " +
        "where date between strftime('%Y-%m-%d',date('now')) " +
        "and strftime('%Y-%m-%d',date('now','+1 day')) group by strftime('%Y-%m-%d', date)";
      return executeSql(queryToday);
    }


    function monthVisit() {
      var queryMonth = "select count() as num from stats  " +
        "where date between strftime('%Y-%m',date('now')) " +
        "and strftime('%Y-%m',date('now','+1 month')) group by strftime('%Y-%m', date);";
      return executeSql(queryMonth);
    }


    function globalVisit() {

      var queryGlobal = "select count() as num from stats;";
      return executeSql(queryGlobal);
    }

    function executeSql(sql) {
      var db = accessDB();
      return $cordovaSQLite.execute(db, sql);
    }

    function accessDB() {
     if (db===null) {
       openDb();
     }
      return db;
    }

  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.account.directives', []);
  angular
    .module('counterMax.account.controllers', []);

  angular
    .module('counterMax.account.services', []);
  angular
    .module('counterMax.account.config', []);
  angular
    .module('counterMax.account', [
      'counterMax.account.controllers',
      'counterMax.account.services',
      'counterMax.account.config',
      'counterMax.account.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.account.controllers')
    .controller('AccountCtrl', AccountCtrl);

  AccountCtrl.$inject = ['$scope','$cordovaPreferences'];

  /**
   * @namespace AccountCtrl
   */
  function AccountCtrl($scope,$cordovaPreferences) {
    var vm = this;
    $scope.settings = {
      enableFriends: true
    };
    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.dashboard.controllers.DashCtrl
     */
    function activate() {
      $cordovaPreferences.show()
        .success(function(value) {
          alert("Success: " + value);
        })
        .error(function(error) {
          alert("Error: " + error);
        });
    }


  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.visitas.directives', []);
  angular
    .module('counterMax.visitas.controllers', []);

  angular
    .module('counterMax.visitas.services', []);
  angular
    .module('counterMax.visitas.config', []);
  angular
    .module('counterMax.visitas', [
      'counterMax.visitas.controllers',
      'counterMax.visitas.services',
      'counterMax.visitas.config',
      'counterMax.visitas.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.visitas.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.visitas.controllers')
    .controller('VisitasCtrl', VisitasCtrl);

  VisitasCtrl.$inject = ['$scope','$ionicPopup','$filter','$cordovaGoogleAds','databaseService'];

  /**
   * @namespace HotelBookingController
   */
  function VisitasCtrl($scope,$ionicPopup,$filter,$cordovaGoogleAds,databaseService) {
    $scope.salirApp=salirApp;
    var $translate = $filter('translate');
    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.visitas.controllers.VisitasCtrl
     */
    function activate() {
      $cordovaGoogleAds.showInterstitial();
      databaseService.todayVisit().then(visitasHoy,function(e){console.log(e);});
      databaseService.monthVisit().then(visitasMes,function(e){console.log(e);});
      databaseService.globalVisit().then(visitasGlobal,function(e){console.log(e);});
    }


    function visitasHoy(res) {
      $scope.visitasHoy=res.rows.item(0).num;
    }
    function visitasMes(res) {
      $scope.visitasMes=res.rows.item(0).num;
    }
    function visitasGlobal(res) {
      $scope.visitasTotal=res.rows.item(0).num;
    }
    /**
     *
     * no funciona en IOS además pueden no validar la app
     *
    function salirApp() {
      var confirmPopup = $ionicPopup.show({
        title : $translate('VISITAS_POPUP_TITLE'),
        template : $translate('VISITAS_POPUP_MENSAJE'),
        buttons : [{
          text : $translate('BUTTON_CANCEL'),
          type : 'button-stable button-outline'
        }, {
          text : $translate('BUTTON_OK'),
          type : 'button-stable',
          onTap : function() {
            databaseService.closeDb();
            ionic.Platform.exitApp();

          }
        }]
      });
    }
    */

  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.dashboard.directives', []);
  angular
    .module('counterMax.dashboard.controllers', []);

  angular
    .module('counterMax.dashboard.services', []);
  angular
    .module('counterMax.dashboard.config', []);
  angular
    .module('counterMax.dashboard', [
      'counterMax.dashboard.controllers',
      'counterMax.dashboard.services',
      'counterMax.dashboard.config',
      'counterMax.dashboard.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.dashboard.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.dashboard.controllers')
    .controller('DashCtrl', DashCtrl);

  DashCtrl.$inject = ['$scope','$cordovaFacebook'];

  /**
   * @namespace HotelBookingController
   */
  function DashCtrl($scope,$cordovaFacebook) {
    $scope.facebookLogin=facebookLogin;

    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.dashboard.controllers.DashCtrl
     */
    function activate() {

    }

    function facebookLogin() {
      $cordovaFacebook.login(["public_profile", "email", "user_friends"])
        .then(function(success) {
         console.log(success);
        }, function (error) {
          console.log(error);
        });

    }



  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.video.directives', []);
  angular
    .module('counterMax.video.controllers', []);

  angular
    .module('counterMax.video.services', []);
  angular
    .module('counterMax.video.config', []);
  angular
    .module('counterMax.video', [
      'counterMax.video.controllers',
      'counterMax.video.services',
      'counterMax.video.config',
      'counterMax.video.directives',
      'uuid'
    ]);

})();

(function () {
  'use strict';
  /* @ngInject */
  videoLaunch.$inject = ['$rootScope', '$timeout'];
  function videoLaunch($rootScope, $timeout) {

    var directive = {
      restrict: 'EA',
      scope: {
        hotel: '=',
      },
      template: '<div class="video-container"></div>',
      link: link,

    };
    return directive;

    function link(scope, element, attributes) {


      function updatePosition() {
        cordova.plugins.phonertc.setVideoView({
          container: element[0],
          local: {
            position: [240, 240],
            size: [50, 50]
          }
        });

      }
    }


  }

  angular
    .module('counterMax.video.directives')
    .directive('videoView', videoLaunch);


})();

/**
 * DashCtrl
 * @namespace counterMax.video.controllers
 *
 *return $cordovaSQLite.execute(db, queryToday).then(function (res) {
        return res.rows.item(0).num;

      }, function (err) {
        console.error(err);
        return -1
      });


 */
(function () {
  'use strict';

  angular.module('counterMax.video.controllers').controller('VideoCtrl', VideoCtrl);
  VideoCtrl.$inject = [
    '$scope',
    '$filter',
    '$sce',
    '$cordovaPreferences',
    '$cordovaNativeAudio',
    '$state',
    '$timeout',
    'databaseService',
    'Animate',
    'UserMedia',
    'VisitService',
    'rfc4122'
  ];

  /**
   * @namespace VideoCtrl
   */
  function VideoCtrl($scope,
                     $filter,
                     $sce,
                     $cordovaPreferences,
                     $cordovaNativeAudio,
                     $state,
                     $timeout,
                     databaseService,
                     Animate,
                     UserMedia,
                     VisitService,
                     rfc4122) {
    var $translate = $filter('translate');
    $scope.sensor1 = 0;
    $scope.sensor2 = 1;
    $scope.inicio = false;
    $scope.abierto = false;
    $scope.stream = null;
    $scope.visitasHoy = 0;
    $scope.visitasMes = 0;
    $scope.visitasTotal = 0;
    $scope.captureVideo = captureVideo;
    $scope.video = document.getElementById('monitor');


    activate();


    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.video.controllers.VideoCtrl
     */
    function activate() {
      loadAnalytics();
      prepareInterface();
      $cordovaPreferences.fetch('timer').success(function (value) {
          if (value === null) {
            value = 10;
          }
          $scope.timer = parseInt(value) * 60 * 1000; // pasamos minutos a milisegundos
        })
        .error(function (error) {
          $scope.timer = 10 * 60 * 1000; // Default 5 minutos
        });
    }

    function loadAnalytics() {

      /**
       * Analitics
       */
      $cordovaPreferences.fetch('analyticsID').success(function (value) {
          console.log("Analytics Value " + value);
          $scope.analyticsID = value;
        })
        .error(function (error) {
          $scope.analyticsID = null;
        });
    }

    function prepareInterface() {
      $scope.buttons = [];
      var button1 = new Image();
      var button2 = new Image();
      var buttonData1 = null;
      var buttonData2 = null;
      button1.src = "img/SquareRed.png";
      button2.src = "img/SquareGreen.png";
      $cordovaPreferences.fetch('dis').success(function (value) {

          if (value === 0) {
            buttonData1 = {name: "red", image: button1, x: 320 - 32, y: 120, w: 32, h: 32};
            buttonData2 = {name: "green", image: button2, x: 0, y: 120, w: 32, h: 32};
          } else {
            buttonData2 = {name: "red", image: button1, x: 320 - 32, y: 120, w: 32, h: 32};
            buttonData1 = {name: "green", image: button2, x: 0, y: 120, w: 32, h: 32};
          }
          $scope.buttons.push(buttonData1);
          $scope.buttons.push(buttonData2);

        })
        .error(function (error) {
          alert("Error: " + error);
        });
    }

    function captureVideo() {
      if ($scope.abierto) {
        $scope.abierto = false;
        Animate.setVideoCanvas(document.getElementById('videoCanvas'));
        $state.go('tab.visitas');
      } else {
        $scope.abierto = true;
        Animate.setVideo($scope.video);
        Animate.setBlendCanvas(document.getElementById("blendCanvas"));
        Animate.setVideoCanvas(document.getElementById('videoCanvas'));
        Animate.setLayer2Canvas(document.getElementById('layer2'));
        Animate.setButtons($scope.buttons);
        UserMedia.start().then(sucessStartAnimation, function (e) {
          console.log("ANIMATION" + e);
        });
      }

    }

    function contabilizarNuevaVisita() {

      var cid = rfc4122.v4();
      if ($scope.analyticsID === null) {
        loadAnalytics();

      }

      var visitObj = {
        v: 1,
        tid: $scope.analyticsID,
        cid: cid,
        t: 'event',
        ec: $translate('EVENTO_CATEGORY'),
        ea: $translate('EVENTO_ACCION'),
        el: $translate('EVENTO_LABEL'),
        ev: 1,
        sc: 'start',
        ds: 'app'
      };

      databaseService.addVisit();
      $cordovaNativeAudio.play('click');
      if ($scope.analyticsID !== null) {
        VisitService.addVisit(visitObj);
        $timeout(destroyVisit, $scope.timer, true, cid);
      }
    }

    function destroyVisit(cid) {
      var destroyObject = {
        v: 1,
        tid: $scope.analyticsID,
        cid: cid,
        sc: 'stop'

      };
      VisitService.destroySession(destroyObject);
    }

    function animate() {
      if ($scope.abierto) {
        Animate.draw().then(drawSuccessFn, function () {
          newFrame();
        });
      }
    }


    function sucessStartAnimation(stream) {
      window.stream = stream; // stream available to console for dev
      if (window.URL) {
        $scope.videostream = $sce.trustAsResourceUrl(window.URL.createObjectURL(stream));
      } else {
        $scope.videostream = $sce.trustAsResourceUrl(stream);
      }
      animate();

    }

    function newFrame() {
      if ($scope.abierto) {
        ionic.requestAnimationFrame(animate);
      }
    }

    function drawSuccessFn(data) {
      if (data.status == 1) {

        Animate.blender().then(blenderSuccessFn, function () {
          console.log("error blender");
        });

      } else {
        newFrame();

      }

    }

    function blenderSuccessFn(data) {

      if (data.status == 1) {
        Animate.checkAreas().then(checkAreasSuccessFn, function () {
          console.log("error areas");
        });

      }
    }

    function checkAreasSuccessFn(data) {

      if (data.status === 1) {
        if (data.btn >= 0) {
          if (data.btn === 0) {
            $scope.inicio = true;
          } else {
            if (( $scope.inicio === true) && (data.btn == 1)) {
              $scope.inicio = false;
              contabilizarNuevaVisita();
            }
          }
        }
      }
      newFrame();
    }


  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('UserMedia', UserMedia);

  UserMedia.$inject = ['$q'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function UserMedia($q) {
    navigator.getUserMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);
    var constraints = {
      audio: false,
      video: {
        mandatory: {
          maxWidth: 320,
          maxHeight: 240
        }
      }
    };

    var deferred = $q.defer();
    var video = null;
    return {
      start: start,
      stop: stop
    };


    function start() {
      navigator.getUserMedia(
        constraints,
        function (stream) {
          video = stream;
          deferred.resolve(stream);
        },
        function errorCallback(error) {
          console.log('navigator.getUserMedia error: ', error);
          deferred.reject(error);
        }
      );

      return deferred.promise;

    }

    function stop() {
      var videoTrack = video.getVideoTracks();
      video.src='';
    }


  }
})();



/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('Animate', Animate);

  Animate.$inject = ['$q'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function Animate($q) {


    var buttons = null;
    var lastImageData = null;
    var video = null;
    var videoCanvas = null;
    var videoCtx = null;
    var layer2Canvas = null;
    var layer2Ctx = null;
    var blendCanvas = null;
    var blendCtx = null;
    return {
      setLayer2Canvas: setLayer2Canvas,
      setVideoCanvas: setVideoCanvas,
      setButtons: setButtons,
      setVideo: setVideo,
      setBlendCanvas: setBlendCanvas,
      draw: draw,
      blender: blender,
      checkAreas: checkAreas
    };


    function setBlendCanvas(obj) {

      blendCanvas = obj;
      blendCtx = blendCanvas.getContext('2d');

    }

    function setVideoCanvas(obj) {
      videoCanvas = obj;
      videoCtx = videoCanvas.getContext('2d');
      // these changes are permanent
      videoCtx.translate(320, 0);
      videoCtx.scale(-1, 1);

// background color if no video present
      videoCtx.fillStyle = '#008e94';
      videoCtx.fillRect(0, 0, videoCanvas.width, videoCanvas.height);
    }

    function setLayer2Canvas(obj) {

      layer2Canvas = obj;
      layer2Ctx = layer2Canvas.getContext('2d');
    }

    function setButtons(obj) {
      buttons = obj;
    }

    function setVideo(obj) {
      video = obj;
    }


    function draw() {
      var result = {status: 0};
      var deferred = $q.defer();
      if (video.readyState === video.HAVE_ENOUGH_DATA) {
        // mirror video
        videoCtx.drawImage(video, 0, 0, videoCanvas.width, videoCanvas.height);
        for (var i = 0; i < buttons.length; i++)
          layer2Ctx.drawImage(buttons[i].image, buttons[i].x, buttons[i].y, buttons[i].w, buttons[i].h);
        result.status = 1;
      }
      deferred.resolve(result);
      return deferred.promise;
    }

    function blender() {
      var result = {status: 0};
      var deferred = $q.defer();
      var width = videoCanvas.width;
      var height = videoCanvas.height;
      // get current webcam image data
      var sourceData = videoCtx.getImageData(0, 0, width, height);
      // create an image if the previous image doesn't exist
      if (!lastImageData) lastImageData = videoCtx.getImageData(0, 0, width, height);
      // create a ImageData instance to receive the blended result
      var blendedData = videoCtx.createImageData(width, height);
      // blend the 2 images
      differenceAccuracy(blendedData.data, sourceData.data, lastImageData.data);
      // draw the result in a canvas
      blendCtx.putImageData(blendedData, 0, 0);
      // store the current webcam image
      lastImageData = sourceData;
      result.status=1;
      deferred.resolve(result);
      return deferred.promise;
    }


    function checkAreas() {
      var result = {status: 0,btn:-1};

      var deferred = $q.defer();
      for (var b = 0; b < buttons.length; b++) {
        // get the pixels in a note area from the blended image
        var blendedData = blendCtx.getImageData(buttons[b].x, buttons[b].y, buttons[b].w, buttons[b].h);

        // calculate the average lightness of the blended data
        var i = 0;
        var sum = 0;
        var countPixels = blendedData.data.length * 0.25;
        while (i < countPixels) {
          sum += (blendedData.data[i * 4] + blendedData.data[i * 4 + 1] + blendedData.data[i * 4 + 2]);
          ++i;
        }
        // calculate an average between of the color values of the note area [0-255]
        var average = Math.round(sum / (3 * countPixels));

        if (average > 50) // more than 20% movement detected

        {
          result.btn = b;
          result.status = 1;
        }

      }

      deferred.resolve(result);
      return deferred.promise;


    }


    function differenceAccuracy(target, data1, data2) {
      if (data1.length != data2.length) return null;
      var i = 0;
      while (i < (data1.length * 0.25)) {
        var average1 = (data1[4 * i] + data1[4 * i + 1] + data1[4 * i + 2]) / 3;
        var average2 = (data2[4 * i] + data2[4 * i + 1] + data2[4 * i + 2]) / 3;
        var diff = threshold(fastAbs(average1 - average2));
        target[4 * i] = diff;
        target[4 * i + 1] = diff;
        target[4 * i + 2] = diff;
        target[4 * i + 3] = 0xFF;
        ++i;
      }
    }

    function fastAbs(value) {
      return (value ^ (value >> 31)) - (value >> 31);
    }

    function threshold(value) {
      return (value > 0x15) ? 0xFF : 0;
    }



  }
})();



/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('VisitService', Visit);

  Visit.$inject = ['$http'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function Visit($http) {

    return {
      addVisit: addVisit,
      destroySession: destroySession

    };


    function addVisit(content) {
      console.log("entro a añadir nueva visita a google");
      return sendPost(content);
    }

    function destroySession(content) {
      return sendPost(content);
    }

    function sendPost(content) {
      return $http({
        url: "http://www.google-analytics.com/collect",
        method: 'POST',
        params: content,
        paramSerializer: '$httpParamSerializerJQLike'
      });

    }


  }
})();

(function () {
  'use strict';
  angular
    .module('counterMax.settings.directives', []);
  angular
    .module('counterMax.settings.controllers', []);

  angular
    .module('counterMax.settings.services', []);
  angular
    .module('counterMax.settings.config', []);
  angular
    .module('counterMax.settings', [
      'counterMax.settings.controllers',
      'counterMax.settings.services',
      'counterMax.settings.config',
      'counterMax.settings.directives'
    ]);

})();

/**
 * DashCtrl
 * @namespace counterMax.settings.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.settings.controllers')
    .controller('SettingsCtrl', SettingsCtrl);

  SettingsCtrl.$inject = ['$scope', '$cordovaPreferences', '$cordovaNativeAudio','$cordovaToast','$filter'];

  /**
   * @namespace AccountCtrl
   */
  function SettingsCtrl($scope, $cordovaPreferences, $cordovaNativeAudio,$cordovaToast,$filter) {

    $scope.settings = {};
    var $translate = $filter('translate');
    $scope.saveOptions = saveOptions;
    $scope.updateSound = updateSound;
    activate();


    function activate() {
      /**
       * Definimos los valores por defecto
       */

      $cordovaPreferences.fetch('analyticsID').success(function (value) {
          $scope.settings.analyticsID = value;
        })
        .error(function (error) {
          alert("Error: " + error);
        });

      $cordovaPreferences.fetch('dis').success(function (value) {
          $scope.settings.disposition = value;
        })
        .error(function (error) {
          alert("Error: " + error);
        });


      $cordovaPreferences.fetch('sound')
        .success(function (value) {
            $scope.settings.sound = value;
        })
        .error(function (error) {
          $scope.settings.sound = 'sounds/Ariel.mp3';

        });
    }

    function updateSound() {
      $cordovaNativeAudio.unload('click').then(successUnloadSound, failUnloadSound);
    }

    function saveOptions() {
      $cordovaPreferences.store('analyticsID', $scope.settings.analyticsID);
      $cordovaPreferences.store('dis', $scope.settings.disposition);
      $cordovaPreferences.store('sound', $scope.settings.sound);
      $cordovaToast.showLongBottom($translate('SETTINGS_SAVE')).then(function (success) {}, function (error) {
        // error
      });
    }

    function successUnloadSound(result) {

      $cordovaNativeAudio.preloadSimple('click', $scope.settings.sound).then(function (msg) {
        $cordovaNativeAudio.play('click');
      }, function (error) {
        alert(error);
      });


    }

    function failUnloadSound(err) {

      $cordovaNativeAudio.preloadSimple('click', $scope.settings.sound).then(function (msg) {
        $cordovaNativeAudio.play('click');
      }, function (error) {
        alert(error);
      });
    }


  }
})();

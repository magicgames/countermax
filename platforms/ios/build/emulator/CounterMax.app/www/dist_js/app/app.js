(function () {
  'use strict';

  angular.module('counterMax', [

    'ionic',
    'counterMax.routes',
    'counterMax.services',
    'counterMax.controllers',
    'counterMax.video',
    'counterMax.dashboard',
    'counterMax.account',
    'counterMax.settings',
    'counterMax.visitas',
    'ngCordova',
    'angular-oauth2',
    'templates',
    'pascalprecht.translate'  // inject the angular-translate module


  ]);
  angular.module('counterMax.services', []);
  angular.module('counterMax.routes', []);
  angular.module('counterMax.controllers', []);
  angular.module('counterMax').config(config);
  config.$inject = ['$ionicConfigProvider', '$translateProvider', 'OAuthProvider'];
  angular.module('counterMax').run(run);
  run.$inject = [
    '$ionicPlatform',
    '$cordovaPushV5',
    '$rootScope',
    '$cordovaToast',
    '$ionicHistory',
    '$state',
    '$translate',
    '$filter',
    '$cordovaGoogleAds',
    'OAuth',
    'databaseService'];

  /**
   * @name config
   * @desc Configure all factories
   * @param $ionicConfigProvider
   * @param $translateProvider
   * @param OAuthProvider
   */
  function config($ionicConfigProvider, $translateProvider, OAuthProvider) {


    OAuthProvider.configure({
      baseUrl: 'http://www.google.es',
      clientId: '1232',
      clientSecret: null,
      grantPath: '/oauth2/token',
      revokePath: '/oauth2/revoke'
    });

    $ionicConfigProvider.navBar.alignTitle('center');
    $translateProvider
      .useStaticFilesLoader({
        prefix: 'locales/',
        suffix: '.json'
      })
      .registerAvailableLanguageKeys(['es', 'en'], {
        'en': 'en', 'en_GB': 'en', 'en_US': 'en',
        'es': 'es', 'es_ES': 'es', 'es_AR': 'es'
      })
      .preferredLanguage('es')
      .fallbackLanguage('en')
      .determinePreferredLanguage()
      .useSanitizeValueStrategy('escapeParameters');
  }


  /**
   * @name run
   */
  function run($ionicPlatform,
               $cordovaPushV5,
               $rootScope,
               $cordovaToast,
               $ionicHistory,
               $state,
               $translate,
               $filter,
               $cordovaGoogleAds,
               OAuth,
               databaseService) {

    var countTimerForCloseApp = false;


    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      if(window.cordova) {
        registerBackButton($ionicPlatform, $cordovaToast, $state, $ionicHistory, $filter, countTimerForCloseApp);
        registerPush($cordovaPushV5, $rootScope);
        databaseService.createTableVisit();
        prepareTranslation($translate);

        oauthErrors($rootScope, OAuth);

        prepareAds($cordovaGoogleAds);

      }
      if (window.localStorage.loginCorrect === "true") {
        $state.go('tab.video');

      } else {

        $state.go('auth');
      }


    });

  }

  /**
   * @name prepareTranlation
   * @desc carga el archivo de traduccion
   * @param $translate
   */
  function prepareTranslation($translate) {
    if (typeof navigator.globalization !== "undefined") {
      navigator.globalization.getPreferredLanguage(function (language) {
        var language_list = {"French": "es", "English": "en", "Spanish": "es", "Chinese": "zh", "Japanese": "ja"};
        var language_value = (language.value).split(/[\s,-]+/)[0];

        if (language_list.hasOwnProperty(language_value)) {
          var language_locale = language_list[language_value];
          $translate.use(language_locale).then(function (data) {
            console.log("SUCCESS -> " + data);
          }, function (error) {
            console.log("ERROR -> " + error);
          });
        }
      }, null);
    }

  }

  /**
   * @name prepareAds
   * @desc prepara la publicidad
   * @param $cordovaGoogleAds
   */
  function prepareAds($cordovaGoogleAds) {
    var admobid = {};
    //Banner configuration
    admobid = {
      banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
      interstitial: 'ca-app-pub-2135463162242237/3946497300'
    };
    if (ionic.Platform.isIOS()) {
      cordova.plugins.iosrtc.registerGlobals();
      admobid = {
        banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2135463162242237/3946497300'
      };
    }
    if (ionic.Platform.isAndroid()) {
      admobid = {
        banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2135463162242237/3946497300'
      };

    }
    if (ionic.Platform.isWindowsPhone()) {
      admobid = {
        banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2135463162242237/3946497300'
      };
    }

    if (ionic.Platform.isWebView()) {
      admobid = {
        banner: 'ca-app-pub-2135463162242237/9993030900', // or DFP format "/6253334/dfp_example_ad"
        interstitial: 'ca-app-pub-2135463162242237/3946497300'
      };
    }
    var banner_options = {
      adSize: 'SMART_BANNER',
      width: 360, // valid when set adSize 'CUSTOM'
      height: 90, // valid when set adSize 'CUSTOM'
      position: 8,
      x: 0,       // valid when set position to POS_XY
      y: 0,       // valid when set position to POS_XY
      isTesting: false,
      autoShow: true
    };
    $cordovaGoogleAds.setOptions(banner_options).then(creobannerOK, creobannerKO);
    function creobannerOK() {

      $cordovaGoogleAds.createBanner(admobid.banner).then(function (ok) {
        $cordovaGoogleAds.showBanner(8);
      }, function (err) {
        console.log('err');
      });
      $cordovaGoogleAds.prepareInterstitial({adId: admobid.interstitial, autoShow: false,isTesting: false});
    }

    function creobannerKO() {
      console.log("No puedo hacer nada");
    }


  }


  function oauthErrors($rootScope, OAuth) {
    $rootScope.$on('oauth:error', function (event, rejection) {
      // Ignore `invalid_grant` error - should be catched on `LoginController`.
      if ('invalid_grant' === rejection.data.error) {
        return;
      }

      // Refresh token when a `invalid_token` error occurs.
      if ('invalid_token' === rejection.data.error) {
        return OAuth.getRefreshToken();
      }

    });

  }

  function registerBackButton($ionicPlatform, $cordovaToast, $state, $ionicHistory, $filter, countTimerForCloseApp) {
    var $translate = $filter('translate');
    $ionicPlatform.registerBackButtonAction(function (e) {
      e.preventDefault();
      function showConfirm() {
        if (countTimerForCloseApp) {
          ionic.Platform.exitApp();
        } else {
          $cordovaToast.showLongBottom($translate('CONFIRM_EXIT')).then(function (success) {
            countTimerForCloseApp = true;
            $timeout(function () {
              countTimerForCloseApp = false;
            }, 2000, true, countTimerForCloseApp);

          }, function (error) {
            // error
          });
        }

      }

      // Is there a page to go back to?
      if (($ionicHistory.backView()) && !($state.is('tab.tutorial') || $state.is('tab.video'))) {
        // Go back in history
        $ionicHistory.backView().go();
      } else {
        // This is the last page: Show confirmation popup
        showConfirm();
      }

      return false;
    }, 101);
  }

  /**
   * Controla el sistema de push
   * @param $cordovaPushV5
   * @param $rootScope
   */
  function registerPush($cordovaPushV5, $rootScope) {

      var pushOptions = {
        android: {
          senderID: "822255661242",
          icon: "icon",
          iconColor: "#008e94"
        },
        ios: {
          alert: "true",
          badge: "true",
          sound: "true"
        },
        windows: {}
      };



      $cordovaPushV5.initialize(pushOptions);
      $cordovaPushV5.register().then(function (token) {
        console.log('$cordovaPushV5:REGISTERED', token);
      }, function (err) {
        console.error('$cordovaPushV5:REGISTER_ERROR', err);
      });


      $rootScope.$on('$cordovaPushV5:notificationReceived', function (event, notification) {
        switch (notification.event) {
          case 'registered':
            if (notification.regid.length > 0) {
              alert('registration ID = ' + notification.regid);
            }
            break;

          case 'message':
            // this is the actual push notification. its format depends on the data model from the push server
            alert('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
            break;

          case 'error':
            alert('GCM error = ' + notification.msg);
            break;

          default:
            alert('An unknown GCM event has occurred');
            break;
        }
      });




  }
})();

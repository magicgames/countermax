(function () {
  'use strict';

  angular
    .module('counterMax.routes')
    .config(config);

  config.$inject = ['$stateProvider','$urlRouterProvider'];

  /**
   * @name config
   * @desc Enable HTML5 routing
   */
  function config($stateProvider, $urlRouterProvider) {



    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
      .state('auth', {
        url: '/auth',
        cache:false,
        templateUrl: 'login.html',
        controller:'DefaultController'
      })

      .state('intro', {
        url: '/',
        cache:false,
        template:'<div></div>',
        controller:'AuthCtrl'
      })



      // setup an abstract state for the tabs directive
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'tabs.html'

      })

      // Each tab has its own nav history stack:

      .state('tab.tutorial', {
        url: '/tutorial',
        views: {
          'tab-tutorial': {
            templateUrl: 'tab-tutorial.html',
            controller: 'DashCtrl'
          }
        }
      })
      .state('tab.visitas', {
        url: '/visitas',
        cache:false,
        views: {
          'tab-visitas': {
            templateUrl: 'tab-visitas.html',
            controller: 'VisitasCtrl'
          }
        }
      })

      .state('tab.video', {
        url: '/video',
        views: {
          'tab-video': {
            templateUrl: 'tab-video.html',
            controller: 'VideoCtrl'
          }
        }
      })
      .state('tab.settings', {
        url: '/settings',
        views: {
          'tab-settings': {
            templateUrl: 'tab-settings.html',
            controller: 'SettingsCtrl'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');


  }
})();

/**
 * DashCtrl
 * @namespace counterMax.dashboard.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.dashboard.controllers')
    .controller('DashCtrl', DashCtrl);

  DashCtrl.$inject = ['$scope','$cordovaFacebook'];

  /**
   * @namespace HotelBookingController
   */
  function DashCtrl($scope,$cordovaFacebook) {
    $scope.facebookLogin=facebookLogin;

    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.dashboard.controllers.DashCtrl
     */
    function activate() {

    }

    function facebookLogin() {
      $cordovaFacebook.login(["public_profile", "email", "user_friends"])
        .then(function(success) {
         console.log(success);
        }, function (error) {
          console.log(error);
        });

    }



  }
})();

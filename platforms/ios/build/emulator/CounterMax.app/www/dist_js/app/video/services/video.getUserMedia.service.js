/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('UserMedia', UserMedia);

  UserMedia.$inject = ['$q'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function UserMedia($q) {
    navigator.getUserMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);
    var constraints = {
      audio: false,
      video: {
        mandatory: {
          maxWidth: 320,
          maxHeight: 240
        }
      }
    };

    var deferred = $q.defer();
    var video = null;
    return {
      start: start,
      stop: stop
    };


    function start() {
      navigator.getUserMedia(
        constraints,
        function (stream) {
          video = stream;
          deferred.resolve(stream);
        },
        function errorCallback(error) {
          console.log('navigator.getUserMedia error: ', error);
          deferred.reject(error);
        }
      );

      return deferred.promise;

    }

    function stop() {
      var videoTrack = video.getVideoTracks();
      video.src='';
    }


  }
})();



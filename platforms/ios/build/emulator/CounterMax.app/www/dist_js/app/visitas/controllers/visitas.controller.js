/**
 * DashCtrl
 * @namespace counterMax.visitas.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.visitas.controllers')
    .controller('VisitasCtrl', VisitasCtrl);

  VisitasCtrl.$inject = ['$scope','$ionicPopup','$filter','$cordovaGoogleAds','databaseService'];

  /**
   * @namespace HotelBookingController
   */
  function VisitasCtrl($scope,$ionicPopup,$filter,$cordovaGoogleAds,databaseService) {
    $scope.salirApp=salirApp;
    var $translate = $filter('translate');
    activate();

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf counterMax.visitas.controllers.VisitasCtrl
     */
    function activate() {
      $cordovaGoogleAds.showInterstitial();
      databaseService.todayVisit().then(visitasHoy,function(e){console.log(e);});
      databaseService.monthVisit().then(visitasMes,function(e){console.log(e);});
      databaseService.globalVisit().then(visitasGlobal,function(e){console.log(e);});
    }


    function visitasHoy(res) {
      $scope.visitasHoy=res.rows.item(0).num;
    }
    function visitasMes(res) {
      $scope.visitasMes=res.rows.item(0).num;
    }
    function visitasGlobal(res) {
      $scope.visitasTotal=res.rows.item(0).num;
    }
    /**
     *
     * no funciona en IOS además pueden no validar la app
     *
    function salirApp() {
      var confirmPopup = $ionicPopup.show({
        title : $translate('VISITAS_POPUP_TITLE'),
        template : $translate('VISITAS_POPUP_MENSAJE'),
        buttons : [{
          text : $translate('BUTTON_CANCEL'),
          type : 'button-stable button-outline'
        }, {
          text : $translate('BUTTON_OK'),
          type : 'button-stable',
          onTap : function() {
            databaseService.closeDb();
            ionic.Platform.exitApp();

          }
        }]
      });
    }
    */

  }
})();

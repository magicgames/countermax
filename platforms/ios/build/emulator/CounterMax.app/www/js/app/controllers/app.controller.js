/**
 * DashCtrl
 * @namespace counterMax.account.controllers
 */
(function () {
  'use strict';

  angular
    .module('counterMax.controllers')
    .controller('DefaultController', DefaultController);

  DefaultController.$inject = ['$scope','$state','$cordovaFacebook'];

  /**
   * @namespace AccountCtrl
   */
  function DefaultController($scope,$state,$cordovaFacebook) {

    $scope.facebookLogin=facebookLogin;
    $scope.normalLogin=normalLogin;
    /*
    activate();
    $scope.facebookLogin=facebookLogin
*/
    window.localStorage.loginCorrect="false";





    function normalLogin() {
      window.localStorage.loginCorrect="true";
      if(window.localStorage.didTutorial === "true") {
        $state.go('tab.video');
      } else {

        $state.go('tab.tutorial');
      }


    }

    function facebookLogin() {
      $cordovaFacebook.login(["public_profile", "email", "user_friends"])
        .then(function(success) {
          window.localStorage.loginCorrect="true";
          if(window.localStorage.didTutorial === "true") {
            $state.go('tab.video');
          } else {

            $state.go('tab.tutorial');
          }



        }, function (error) {
          console.log(error);
        });

    }




  }
})();

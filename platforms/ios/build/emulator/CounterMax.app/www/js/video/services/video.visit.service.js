/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.video.services')
    .factory('VisitService', Visit);

  Visit.$inject = ['$http'];

  /**
   * @namespace counterMax.video.services
   * @returns {Factory}
   */
  function Visit($http) {

    return {
      addVisit: addVisit,
      destroySession: destroySession

    };


    function addVisit(content) {
      console.log("entro a añadir nueva visita a google");
      return sendPost(content);
    }

    function destroySession(content) {
      return sendPost(content);
    }

    function sendPost(content) {
      return $http({
        url: "http://www.google-analytics.com/collect",
        method: 'POST',
        params: content,
        paramSerializer: '$httpParamSerializerJQLike'
      });

    }


  }
})();

/**
 * hotelbeds
 * @namespace counterMax.chat.services
 */
(function () {
  'use strict';

  angular
    .module('counterMax.services')
    .factory('storageService', storageService);

  storageService.$inject = ['$window'];

  /**
   * @namespace counterMax.app.services
   * @returns {Factory}
   */
  function storageService($window) {

    return {
      set:set,
      get:get,
      removeKey:removeKey,
      setObject:setObject,
      getObject:getObject

    };

    function set(key,value) {
      $window.localStorage[key] = value;
    }

    function get(key,defaultValue) {
      return $window.localStorage[key] || defaultValue;
    }

    function removeKey(key) {
      return $window.removeItem[key];
    }
    function setObject(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    }

    function getObject(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }

  }
})();
